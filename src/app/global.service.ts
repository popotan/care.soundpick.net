import { Injectable } from '@angular/core';

@Injectable()
export class GlobalService {

  constructor() { }

  is_loading: boolean = false;
  page_title: string = '사운드픽 케어서비스';

  getCookie() {
    if (document.cookie) {
      let cookie = document.cookie;
      let cookieDict = new Object();
      let cookieStrArr = cookie.split(';');
      for (let cookieIdx in cookieStrArr) {
        let cookieKey = cookieStrArr[cookieIdx].split('=')[0];
        let cookieVal = cookieStrArr[cookieIdx].split('=')[1];
        cookieDict[cookieKey] = cookieVal;
      }
      return cookieDict;
    } else {
      return null;
    }
  }
}
