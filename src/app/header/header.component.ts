import { Title } from '@angular/platform-browser';
import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart, NavigationEnd } from '@angular/router';
import { SessionService } from '../session.service';

declare var jQuery: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    private titleService: Title,
    public session: SessionService,
    public route: ActivatedRoute,
    public router: Router,
    public el: ElementRef) {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationStart) {
        window.scrollTo(0, 0);
      }
      if (val instanceof NavigationEnd) {
        //네비게이션 접기
        this.nav_switch = false;

        //세션 갱신
        this.session.getSession();
      }
    });
  }

  ngOnInit() {
  }

  nav_switch: boolean = false;

  slideTo(placeTo) {
    jQuery('html, body').animate({ scrollTop: jQuery(placeTo).offset().top }, 500);
  }

  // originHeight = this.el.nativeElement.offsetHeight;
  // @HostListener('window:scroll', ['$event'])
  // headerHeight($event) {
  //   let scrollTop = $event.target.scrollingElement.scrollTop;

  //   let targetHeight = this.originHeight - scrollTop;
  //   console.log(this.originHeight, scrollTop, targetHeight);
  //   if (targetHeight < (1 / 16) * 6 * 14) { // 6rem보다 작으면
  //     this.el.nativeElement.style.height = '6rem';
  //   } else {
  //     this.el.nativeElement.style.height = targetHeight.toString() + 'px';
  //   }
  // }
}
