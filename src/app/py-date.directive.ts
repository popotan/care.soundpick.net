import { Directive, Input, Output, AfterViewInit, OnInit, ElementRef } from '@angular/core';

@Directive({
  selector: 'pydate',
  inputs : ['datestr']
})
export class PyDateDirective implements OnInit{

  constructor(private el: ElementRef) { }

  ngOnInit(){
    this.convert();
  }

  @Input() datestr:string;
  rslt: string = '0000년 00월 00일';

  _date_map = {
    'Mon' : '월','Tue' : '화','Wed' : '수','Thu' : '목','Fri' : '금','Sat' : '토','Sun' : '일'
  };
  _month_map = {
    'Jan' : '1','Feb' : '2','Mar' : '3','Apr' : '4','May' : '5','Jun' : '6','Jul':'7','Aug' : '8','Sep' : '9','Oct' : '10','Nov' : '11','Dec' : '12'
  };

  pydate_regexp = new RegExp(/(\w*)[,][ ](\d*)[ ](\w*)[ ](\d*)[ ](\d*)[:](\d*)[:](\d*)[ ](\w*)/, 'im');

  convert(){
    let match = this.pydate_regexp.exec(this.datestr);
    if(match){
      let _day = match[1];
      let _date = match[2];
      let _month = match[3];
      let _year = match[4];
      let _hour = match[5];
      let _minute = match[6];
      let _second = match[7];
      let _timezone = match[8];

      this.rslt = _year + '년 ' + this._month_map[_month] + '월 ' + _date + '일';
      this.el.nativeElement.innerHTML = this.rslt;
    }
  }
}
