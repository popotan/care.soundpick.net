import { Component, OnInit } from '@angular/core';

import { SessionService } from '../../session.service';

declare var daum:any;

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  constructor(public session: SessionService) { }

  ngOnInit() {
  }

  loadPostcodePage(){
    let p = new Promise((resolve, reject) => {
      daum.postcode.load(function(){
        new daum.Postcode({
            oncomplete: function(data: object) {
                if(data){
                  resolve(data);
                }else{
                  reject(data);
                }
            }
        }).open();
      });
    });

    p.then(
      result => {
      },
      error => {
        alert('주소찾기에 오류가 있었습니다. 다시 시도해 주세요!');
      }
    );
    return false;
  }
}
