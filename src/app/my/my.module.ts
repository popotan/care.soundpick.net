import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MyComponent } from './my.component';
import { InfoComponent } from './info/info.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
    {
      path : '',
      component : MyComponent
    },{
      path : 'info',
      component : InfoComponent,
      data : {
        title : '가입정보',
        meta : [
          {property : 'og:title', content : '가입정보'},
          {property : 'og:description', content : '내 가입정보를 확인할 수 있습니다.'},
          {name : 'description', content : '내 가입정보를 확인할 수 있습니다.'}
        ]
    }
    },{
      path : 'care',
      redirectTo : '/care/manage'
    }])
  ],
  declarations: [MyComponent, InfoComponent]
})
export class MyModule { }
