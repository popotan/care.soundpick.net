import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { Router, NavigationEnd } from '@angular/router';
import { trigger, transition, style, animate, state } from '@angular/core';

import { GlobalService } from './global.service';
import { SessionService } from './session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations : [trigger('isLoading',[
    transition(':leave', [
      style({opacity : 1}),
      animate('1s ease-out', style({opacity : 0}))
    ])
  ])]
})
export class AppComponent implements OnInit {
  constructor(
    public titleService: Title,
    public metaService: Meta,
    public router: Router,
    public session: SessionService,
    public global: GlobalService
  ) {
    
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        let title_arr = this.getTitle(router.routerState, router.routerState.root);
        titleService.setTitle(title_arr[0]);

        let meta_arr = this.getMeta(router.routerState, router.routerState.root);
        metaService.addTags(meta_arr[0]);
      }
    });
  }

  ngOnInit() {
    this.session.getSession()
    .then(result => {
      this.global.is_loading = false;
    });
  }

  /**
   * @description meta 태그 생성, title
   * @param {*} state
   * @param {*} parent
   * @returns
   * @memberof AppComponent
   */
  getTitle(state, parent) {
    let data = [];
    if (parent && parent.snapshot.data && parent.snapshot.data.title) {
      data.push(parent.snapshot.data.title);
    }

    if (state && parent) {
      data.push(... this.getTitle(state, state.firstChild(parent)));
    }
    if (data.length > 0) {
      return data;
    } else {
      return ['사운드픽 케어서비스'];
    }
  }

  /**
   * @description meta 태그 생성, 각 모듈에 content 값이 포함되어있음.
   * @param {*} state
   * @param {*} parent
   * @returns
   * @memberof AppComponent
   */
  getMeta(state, parent) {
    let data = [];
    if (parent && parent.snapshot.data && parent.snapshot.data.meta) {
      let children = [];
      parent.snapshot.data.meta.forEach(metaTag => {
        let propertyName = 'name';
        if(metaTag.hasOwnProperty('property')){
          propertyName = 'property';
        }

        let is_tag_exist = this.metaService.getTag(propertyName + '="' + metaTag.name + '"');
        
        if (typeof is_tag_exist != null) {
          this.metaService.removeTag(propertyName + '="' + metaTag.name + '"');
        }
        children.push(metaTag);
      });
      data.push(children);
    }

    if (state && parent) {
      data.push(... this.getMeta(state, state.firstChild(parent)));
    }
    return data;
  }
}