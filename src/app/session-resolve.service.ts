import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Rx';

import { GlobalService } from './global.service';
import { SessionService } from './session.service';

@Injectable()
export class SessionResolveService implements Resolve<any>{

  constructor(
    private global: GlobalService,
    private session: SessionService) { }

  resolve(route: ActivatedRouteSnapshot) : Promise<any>{
    this.global.is_loading = true;
    return new Promise((resolve, reject) => {
      this.session.getSession().then(
        result => {
          this.global.is_loading = false;
          resolve(result);
        },
        error => reject(error)
      )
    });
  }
}
