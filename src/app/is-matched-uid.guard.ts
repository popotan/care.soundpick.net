import { Injectable } from '@angular/core';
import { 
  CanActivate,
  CanActivateChild, 
  ActivatedRouteSnapshot, 
  RouterStateSnapshot, 
  Route,
  Router } from '@angular/router';

import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { GlobalService } from './global.service';

@Injectable()
export class IsMatchedUidGuard implements CanActivate, CanActivateChild {

  constructor(
    private global: GlobalService,
    private router: Router,
    private $http: Http
  ){ }

  canActivate(next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.qualify(next.params['id'], this.router.url).then(
      result => {
        return true;
      }, error => {
        console.error(error);
        return false;
      });
    }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.canActivate(next, state);
  }

  qualify(pkey_id, redirectTo){
    this.global.is_loading = true;

    return new Promise((resolve, reject) => 
    this.getQualification(pkey_id)
    .subscribe(
      result => {
        this.global.is_loading = false;
        if(result['message'] == 'QUALIFIED_FOR_TEST'){
          // alert('이 제품키는 테스트를 위해 별도로 발급된 제품키입니다.');
          resolve(true);
        }else if(result['message'] == 'QUALIFIED'){
          resolve(true);
        }else if(result['message'] == 'UNQUALIFIED'){
          alert('제품키를 가진 본인이 아닙니다.');
          resolve(false);
        }else if(result['message'] == 'USER_SESSION_IS_NOT_EXIST'){
          alert('인증되지 않은 사용자입니다. 로그인페이지로 이동합니다.');
          redirectTo = encodeURI(redirectTo);
          this.router.navigate(['/login'], { queryParams : { r_url : redirectTo } });
          resolve(false);
        }else{
          reject(false);
        }
      }, error => {
        console.error(error);
        alert('제품키 인증 과정에 오류가 발생하였습니다. 다시 시도해주세요.');
        reject(false);
      })
    );
  }

  getQualification(pkey_id){
    return this.$http.post('/api/serial/' + pkey_id + '/qualification', {})
    .map((res: Response) => res.json())
  }
}
