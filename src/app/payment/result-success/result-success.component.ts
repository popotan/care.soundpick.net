import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { GlobalService } from '../../global.service';
import { SessionService } from '../../session.service';
@Component({
  selector: 'app-result-success',
  templateUrl: './result-success.component.html',
  styleUrls: ['./result-success.component.css']
})
export class ResultSuccessComponent implements OnInit {

  constructor(
    private global: GlobalService,
    public session: SessionService,
    private route: ActivatedRoute) { 
    }

  resp: any;

  ngOnInit() {
    this.route.data.subscribe(
      result => {
        this.resp = result['resp'];
        console.log(this.resp);
      }, error => {
        alert('결제정보를 가져오는데 문제가 발생하였습니다.\r\n이 메세지는 정상적으로 결제가 이루어졌으나, 아직 PG사로부터 정산결과를 받아오지 못하여 나타날 가능성이 큽니다.\r\n페이지를 새로고침하시면 결제결과가 나타납니다.');
      });
    // this.resp = this.route.snapshot.data['resp'];
  }

  _pay_method_dict = {
      "vbank" : "무통장입금",
      "trans" : "계좌이체",
      "card" : "신용카드",
      "phone" : '휴대폰 소액결제'
    };
}
