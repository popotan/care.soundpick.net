import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-result-fail',
  templateUrl: './result-fail.component.html',
  styleUrls: ['./result-fail.component.css']
})
export class ResultFailComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  _pay_method_dict = {
      "vbank" : "무통장입금",
      "trans" : "계좌이체",
      "card" : "신용카드",
      "phone" : '휴대폰 소액결제'
    };

}
