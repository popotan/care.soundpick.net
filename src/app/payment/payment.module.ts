import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ImpRespResolveService } from './imp-resp-resolve.service';

import { ResultSuccessComponent } from './result-success/result-success.component';
import { ResultFailComponent } from './result-fail/result-fail.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      {
        path : 'result/fail',
        component : ResultFailComponent
      },{
        path : 'result/:resp_id',
        component : ResultSuccessComponent,
        resolve: {
          resp: ImpRespResolveService
        }
      },{
        path : '**',
        redirectTo : 'result/fail'
      }
    ])
  ],
  declarations: [ResultSuccessComponent, ResultFailComponent],
  providers : [ImpRespResolveService]
})
export class PaymentModule { }
