import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { handleError } from '../async-handling.observable';

@Injectable()
export class ImpRespResolveService implements Resolve<any>{

  constructor(
    private $http: Http,
    private router: Router) { }

  resolve(route: ActivatedRouteSnapshot): Promise<any> | boolean {
    let resp_id = +route.params['resp_id'];
    return new Promise(
      (resolve, reject) => 
      this.$http.get('/api/payment/imp/resp/' + resp_id).map((res: Response) => res.json()).catch(handleError)
        .subscribe(result => {
          resolve(result);
        }, error => {
          alert('결제정보를 가져오는데 문제가 발생하였습니다.');
          return false;
        }));
  }
}
