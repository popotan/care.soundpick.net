import { Component, OnInit } from '@angular/core';

import { SessionService } from '../session.service';

@Component({
  selector: 'app-user-state',
  templateUrl: './user-state.component.html',
  styleUrls: ['./user-state.component.css']
})
export class UserStateComponent implements OnInit {

  constructor(public session: SessionService) { }

  ngOnInit() {
    this.session.getSession();
  }
}