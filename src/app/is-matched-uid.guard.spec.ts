import { TestBed, async, inject } from '@angular/core/testing';

import { IsMatchedUidGuard } from './is-matched-uid.guard';

describe('IsMatchedUidGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsMatchedUidGuard]
    });
  });

  it('should ...', inject([IsMatchedUidGuard], (guard: IsMatchedUidGuard) => {
    expect(guard).toBeTruthy();
  }));
});
