import { Component, OnInit } from '@angular/core';

import { SessionService } from '../../session.service';
@Component({
  selector: 'app-naver',
  templateUrl: './naver.component.html',
  styleUrls: ['./naver.component.css']
})
export class NaverComponent implements OnInit {

  constructor(public session: SessionService) { }

  ngOnInit() {
    this.getStatusToken();
  }


  _status_token:string;

  getStatusToken(){
    this.session.getStatusToken('naver').subscribe(
      result => {
        this._status_token = result.token;
      },
      error => console.error(error)
    );
  };

  issueNaverLoginURL(){
    var url:string = "https://nid.naver.com/oauth2.0/authorize?client_id=mtew_n00aY7MvmXLOyPF&response_type=code&redirect_uri=";
    url += encodeURI('http://care.soundpick.net/api/user/login/naver');
    url += '&state=' + this._status_token;
    return url;
  }

  openLoginDialog(social: string){
    var targetUrl:string = this.issueNaverLoginURL();
    window.location.href = targetUrl;
  }
}
