import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-facebook',
  templateUrl: './facebook.component.html',
  styleUrls: ['./facebook.component.css']
})
export class FacebookComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  issueFacebookLoginURL(){
    var url:string = 'https://www.facebook.com/v2.8/dialog/oauth?client_id=243422466138379';
    url += '&redirect_uri=';
    url += encodeURI('http://care.soundpick.net/api/user/login/facebook');
    url += '&scope=public_profile,email&response_type=code';
    return url;
  }

  openLoginDialog(social: string){
    var targetUrl:string = this.issueFacebookLoginURL();
    window.location.href = targetUrl;
  }
}
