import { TestBed, inject } from '@angular/core/testing';

import { PhoneValidService } from './phone-valid.service';

describe('PhoneValidService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PhoneValidService]
    });
  });

  it('should ...', inject([PhoneValidService], (service: PhoneValidService) => {
    expect(service).toBeTruthy();
  }));
});
