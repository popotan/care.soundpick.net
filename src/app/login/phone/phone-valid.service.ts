import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { handleError } from '../../async-handling.observable';

// declare var IMP: any;

@Injectable()
export class PhoneValidService {

  constructor(private $http: Http) { }

  phone: string;

  // requestIMP(): Promise<boolean>|boolean{
  //   return new Promise((resolve, reject) => {
  //     IMP.init('imp31268121');
  //     IMP.certification({
  //       merchant_uid: ''
  //     }, function (rsp) {
  //       if (rsp.success) {
  //         this.sendIMPcertificationResult(rsp)
  //           .subscribe(result => {
  //             reject(true);
  //           }, error => {
  //             alert('본인인증결과 전송에 실패하였습니다.');
  //             reject(false);
  //           });
  //       } else {
  //         alert('본인인증에 실패하였습니다.');
  //         reject(false);
  //       }
  //     });
  //   });
  // }

  // sendIMPcertificationResult(result) {
  //   return this.$http.post('/api/user/valid/phone', result)
  //     .map((res: Response) => res.json()).catch(handleError)
  // }

  sendValidNumber(phone_number) {
    this.phone = phone_number;
    return this.$http.post('/api/user/valid/phone', { phone: phone_number })
      .map((res: Response) => res.json()).catch(handleError)
  }

  makeAnonymousSession(receive_valid_number) {
    return this.$http.post('/api/user/session/anonymous', { phone: this.phone, valid_number: receive_valid_number })
      .map((res: Response) => res.json()).catch(handleError)
  }
}