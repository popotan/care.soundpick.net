import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {Observable} from 'rxjs/Rx';

import { PhoneValidService } from './phone-valid.service';

@Component({
  selector: 'app-phone',
  templateUrl: './phone.component.html',
  styleUrls: ['./phone.component.css'],
  providers : [PhoneValidService]
})
export class PhoneComponent implements OnInit {

  constructor(
    private phoneValidService : PhoneValidService,
    private router : Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(
      params => { 
        if(params.hasOwnProperty('r_url')){
          this._redirect_url = decodeURIComponent(params['r_url']);
        }else{
          this._redirect_url = '/';
        }
      }
    );
  }

  _phone_number: string;
  _valid_number: string;
  _time: number;
  timer:any;
  _valid_input_visible: boolean = false;
  _is_requested:boolean = false;

  _redirect_url: any;

  requestValidNumber(){
    this.phoneValidService.sendValidNumber(this._phone_number)
    .subscribe(result => {
      this._is_requested = true;
      this._valid_input_visible = true;
      this._time = 300;
      this.timer = Observable.timer(0, 1000);
      this.timer.subscribe(t => {
        if(this._time > 0){
          this._time = this._time-1;
        }
      });
    });
  }

  valid(){
    // this.timer.unsubscribe();
    this.phoneValidService.makeAnonymousSession(this._valid_number)
    .subscribe(result => {
      if(result['status'] == 201){
        this.router.navigateByUrl(this._redirect_url);
      }else{
        alert('인증번호가 유효하지 않습니다. 다시 시도해주세요.');
      }
    });
  }
}