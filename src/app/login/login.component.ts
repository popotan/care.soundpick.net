import { Component, OnInit, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { ActivatedRoute, Params } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import { GlobalService } from '../global.service';
import { SessionService } from '../session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private global: GlobalService,
    public session: SessionService,
    public route: ActivatedRoute,
    private $http: Http
    ) { }

  ngOnInit() {
    let cookie = this.global.getCookie();
    if(cookie === null || !cookie.hasOwnProperty('sp_care')){
      alert('브라우저 쿠키를 허용해주세요.\r\n쿠키를 허용하지 않을 경우, 로그인 상태유지가 불가능합니다.');
    }

    this.route.queryParams.subscribe(
      result => {
        if(result.hasOwnProperty('r_url')){
          this._is_redirect = true;
          this.sendRedirectUrl(result['r_url']);
        }else{
          this.sendRedirectUrl(encodeURI('/'));
        }
      }
    );
  }

  _is_redirect: boolean = false;

  sendRedirectUrl(r_url){    
    this.$http.post('/api/user/redirect_url', {redirect_url : decodeURI(r_url)})
    .map((res: Response) => res.json())
    .subscribe(result => {
      if(result['status'] != 201){
        alert('리다이렉트 주소를 전송하는데 실패하였습니다. 로그인 시, 초기페이지로 이동합니다.');
      }
    });
  }
}
