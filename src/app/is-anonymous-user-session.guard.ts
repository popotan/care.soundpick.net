import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot, Route } from '@angular/router';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { SessionService } from './session.service';

@Injectable()
export class IsAnonymousUserSessionGuard implements CanActivate, CanLoad {

  constructor(
    private session: SessionService,
    private router: Router
  ){ }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return !this.is_anonymous_user();
  }

  canLoad(
    route: Route
  ) : Observable<boolean> | Promise<boolean> | boolean {
    return !this.is_anonymous_user();
  }

  is_anonymous_user(){
    if(this.session._is_logged_in){
        if(this.session._info.division == 'GUEST'){
          return true;
        }else{
          return false;
        }
      }else{
        return false;
      }
  }
}
