import { TestBed, inject } from '@angular/core/testing';

import { SessionResolveService } from './session-resolve.service';

describe('SessionResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SessionResolveService]
    });
  });

  it('should ...', inject([SessionResolveService], (service: SessionResolveService) => {
    expect(service).toBeTruthy();
  }));
});
