import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot, Route } from '@angular/router';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { SessionService } from './session.service';
@Injectable()
export class IsLoggedInGuard implements CanActivate, CanLoad{

  constructor(private session: SessionService, private router: Router){ }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.getUSerSession();
  }

  canLoad(
    route : Route
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.getUSerSession();
  }

  getUSerSession(){
    return this.session.getSession().then(
        result => {
          if(this.session._is_logged_in){
            return true;
          }else{
            alert("로그인이 필요한 메뉴입니다.");
            let redirectTo = encodeURIComponent(this.router.url);
            this.router.navigate(['/login'],{ queryParams : { r_url : redirectTo } });
            return false;
          }
        },
        error => {
          alert("로그인이 필요한 메뉴입니다.");
            let redirectTo = encodeURIComponent(this.router.url);
            this.router.navigate(['/login'],{ queryParams : { r_url : redirectTo } });
            return false;
        });
  }
}
