import { TestBed, async, inject } from '@angular/core/testing';

import { IsAnonymousUserSessionGuard } from './is-anonymous-user-session.guard';

describe('IsAnonymousUserSessionGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsAnonymousUserSessionGuard]
    });
  });

  it('should ...', inject([IsAnonymousUserSessionGuard], (guard: IsAnonymousUserSessionGuard) => {
    expect(guard).toBeTruthy();
  }));
});
