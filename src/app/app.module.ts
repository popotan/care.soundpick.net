import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { SessionService } from './session.service';
import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { UserStateComponent } from './user-state/user-state.component';
import { LoadingComponent } from './loading/loading.component';
import { SitemapComponent } from './sitemap/sitemap.component';

import { GlobalService } from './global.service';
import { IsLoggedInGuard } from './is-logged-in.guard';
import { IsMatchedUidGuard } from './is-matched-uid.guard';
import { IsAnonymousUserSessionGuard } from './is-anonymous-user-session.guard';

import { SessionResolveService } from './session-resolve.service';
import { FacebookComponent } from './login/facebook/facebook.component';
import { NaverComponent } from './login/naver/naver.component';
import { PhoneComponent } from './login/phone/phone.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    UserStateComponent,
    LoadingComponent,
    SitemapComponent,
    FacebookComponent,
    NaverComponent,
    PhoneComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([{
      path: '',
      component: IndexComponent
    }, {
      path: 'sitemap',
      component: SitemapComponent
    }, {
      path: 'login',
      component: LoginComponent,
      data: {
        title: '로그인',
        meta: [
          { name: 'description', content: '사운드픽 케어 서비스에 로그인할 수 있습니다.' },
          { name: 'robots', content: 'index,follow' }
        ]
      }
    }, {
      path: 'document',
      loadChildren: 'app/document/document.module#DocumentModule'
    }, {
      path: 'serial',
      loadChildren: 'app/serial/serial.module#SerialModule'
    }, {
      path: 'payment',
      loadChildren: 'app/payment/payment.module#PaymentModule'
    }, {
      path: 'my',
      loadChildren: 'app/my/my.module#MyModule',
      canLoad: [IsLoggedInGuard]
    }])
  ],
  providers: [
    Title,
    GlobalService,
    IsLoggedInGuard,
    IsAnonymousUserSessionGuard,
    IsMatchedUidGuard,
    SessionService,
    SessionResolveService],
  bootstrap: [AppComponent]
})
export class AppModule { }
