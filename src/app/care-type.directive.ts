import { Directive, Input, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: 'caretype',
  inputs : ['typestr']
})
export class CareTypeDirective implements OnInit{

  constructor(private el: ElementRef) { }

  ngOnInit(){
    this.convert();
  }

  @Input() typestr:string;
  rslt: string = '미가입';

  _type_map = {
    'INVALID' : '일반/미인증',
    'BASIC' : '사픽케어 베이직',
    'PLUS' : '사픽케어 플러스'
  };

  convert(){
    if(this._type_map.hasOwnProperty(this.typestr)){
      this.rslt = this._type_map[this.typestr];
    }else{
      this.rslt = '케어타입정보를 가져오는데 오류가 발생했습니다.';
    }    
    this.el.nativeElement.innerHTML = this.rslt;
  }
}
