import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Rx';

import { PkeyService } from './pkey.service';
import { Pkey } from '../pkey';
@Injectable()
export class InfoResolveService implements Resolve<any> {

  constructor(private target: PkeyService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot): Promise<any> {
    let id = +route.params['id'];
    return new Promise((resolve, reject) => {
      this.target.get(id).then(
        result => {
          this.target.pkey = result;
          resolve(result);
        }, error => {
          this.target.pkey = null;
          reject(error);
        }
      );
    });
  }
}