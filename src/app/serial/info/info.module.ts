import { Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

import { InfoComponent } from './info.component';

import { PkeyService } from './pkey.service';
import { IsLoggedInGuard } from '../../is-logged-in.guard';
import { InfoResolveService } from './info-resolve.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([
      {
        path : ':id/care',
        loadChildren : 'app/serial/info/care/care.module#CareModule'
      },
      {
        path : ':id',
        component : InfoComponent,
        canActivate : [IsLoggedInGuard],
        data : { 
          title : '제품번호 정보',
          meta : [
            {property : 'og:title', content : '제품번호 정보'},
            {property : 'og:description', content : '제품 정보를 확인하고, 사운드픽케어가 제공하는 각종 서비스를 받아보실 수 있습니다.'},
            {name :'description', content : '제품 정보를 확인하고, 사운드픽케어가 제공하는 각종 서비스를 받아보실 수 있습니다.'}
          ]
        },
        resolve : {
          target : InfoResolveService
        }
      }
    ])
  ],
  declarations: [InfoComponent],
  providers : [ PkeyService, InfoResolveService ]
})
export class InfoModule { }
