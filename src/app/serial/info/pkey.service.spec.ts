import { TestBed, inject } from '@angular/core/testing';

import { PkeyService } from './pkey.service';

describe('PkeyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PkeyService]
    });
  });

  it('should ...', inject([PkeyService], (service: PkeyService) => {
    expect(service).toBeTruthy();
  }));
});
