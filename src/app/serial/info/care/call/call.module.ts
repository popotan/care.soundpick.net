import { Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

import { CallService } from './call.service';
import { CallComponent } from './call.component';
import { StateComponent } from './state/state.component';
import { InfoComponent } from './state/info/info.component';

import { InfoResolveService } from '../../info-resolve.service';
import { IsMatchedUidGuard } from '../../../../is-matched-uid.guard';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([{
      path : '',
      component : CallComponent,
      canActivate : [IsMatchedUidGuard],
      resolve : {
        target : InfoResolveService
      }
    },{
      path : 'state',
      component : StateComponent,
      canActivate : [IsMatchedUidGuard]
    }])
  ],
  declarations: [CallComponent, StateComponent, InfoComponent],
  providers : [CallService]
})
export class CallModule { }
