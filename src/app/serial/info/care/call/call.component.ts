import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SerialService } from '../../../serial.service';
import { CallService } from './call.service';

declare var daum:any;

@Component({
  selector: 'app-call',
  templateUrl: './call.component.html',
  styleUrls: ['./call.component.css']
})
export class CallComponent implements OnInit {

  constructor(public serialService : SerialService,
              public callService : CallService,
              private router: Router) { }

  ngOnInit() {
    // if(this.serialService._is_posted && this.serialService._is_valid && this.serialService._string_serial){
    //   this.callService._info.target_serial = this.serialService._string_serial;
    // }
  }

  _addr_hist:[any];

  post(){
    this.callService.postCallInfo()
    .subscribe(result => {
      if(result['state'] == 201){
        this.router.navigate(['./state']);
      }else{
        alert('');
      }
    }, error => {
      console.error(error);
      alert('');
    });
  }

  getAddrHist(){
    this.callService.getAddressHistory()
    .subscribe(result => {
      this._addr_hist = result['response'];
    });
  }

  loadPostcodePage(){
    let p = new Promise((resolve, reject) => {
      daum.postcode.load(function(){
        new daum.Postcode({
            oncomplete: function(data: object) {
                if(data){
                  resolve(data);
                }else{
                  reject(data);
                }
            }
        }).open();
      });
    });

    p.then(
      result => {
        this.callService._info.province = result['address'];
        this.callService._info.postcode = result['postcode'];
      },
      error => {
        alert('주소찾기에 오류가 있었습니다. 다시 시도해 주세요!');
      }
    );
    return false;
  }

}
