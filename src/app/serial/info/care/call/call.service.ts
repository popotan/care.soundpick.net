import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { handleError } from '../../../../async-handling.observable';

import { PkeyService } from '../../pkey.service';
@Injectable()
export class CallService {

  constructor(
    private $http: Http,
    private pkeyService: PkeyService) { }

  _info = {
    user_addr_hist_id : 0,
    province : '',
    avenue : '',
    postcode : '',
    reason : '',
    phone1 : '',
    phone2 : ''
  };

  postCallInfo(){
    return this.$http.post('/api/care/call/'+ this.pkeyService.pkey.pkey_id, this._info)
    .map((res: Response) => res.json()).catch(handleError)
  }

  getAddressHistory(){
    return this.$http.get('/api/user/address')
    .map((res: Response) => res.json()).catch(handleError)
  }
}
