import { TestBed, async, inject } from '@angular/core/testing';

import { IsReceiptGuard } from './is-receipt.guard';

describe('IsReceiptGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsReceiptGuard]
    });
  });

  it('should ...', inject([IsReceiptGuard], (guard: IsReceiptGuard) => {
    expect(guard).toBeTruthy();
  }));
});
