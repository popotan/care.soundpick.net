import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { handleError } from '../../../async-handling.observable';

import { Router, ActivatedRoute } from '@angular/router';

import { SessionService } from '../../../session.service';
import { SerialService } from '../../serial.service';

declare var IMP: any;

@Injectable()
export class PgService {
  _is_loading: boolean;
  _agree: Array<boolean>;
  _info: any;
  _result: any;
  _pay_method_dict = {
    'vbank': '무통장입금',
    'trans': '계좌이체',
    'card': '신용카드',
    'phone': '휴대폰 소액결제'
  };

  constructor(private $http: Http,
    public serialService: SerialService,
    public session: SessionService,
    private route: ActivatedRoute,
    private router: Router) {
    this._is_loading = false;
    this._agree = [false, false, false];
    this._info = {
      pg: 'uplus', // version 1.1.0부터 지원.
      pay_method: 'card',
      merchant_uid: '',
      name: '사운드픽:사픽케어',
      amount: 0,
      buyer_name: this.session._info.name,
      buyer_email: this.session._info.email,
      custom_data: {
        pkey_id: 0,
        care_order_id: 0,
        user_uid: 0,
        user_div: '',
        customer_phone: ''
      },
      digital: true,
      vbank_due: '',
      biz_num: '5405500037',
      m_redirect_url: 'http://care.soundpick.net/api/payment/imp/mobile/complete'
    };
  }

  validInfo() {
    return new Promise((resolve, reject) => {
      if (this.session._is_logged_in) {
        this._info.custom_data.user_uid = this.session._info.user_uid;
        this._info.custom_data.user_div = this.session._info.division;
      } else {
        alert('로그인 세션이 존재하지 않습니다.\r\n로그인 페이지로 이동합니다.');
        this.router.navigate(['/login']);
      }

      for (const bool of this._agree) {
        if (!bool) {
          alert('동의하지 않은 약관이 있습니다.');
          resolve(false);
        }
      }
      if (!this._info.buyer_name || this._info.buyer_name === '') {
        alert('입금/구매자명을 입력해주세요!');
        resolve(false);
      }
      if (!this._info.custom_data.customer_phone || this._info.custom_data.customer_phone == '') {
        alert('연락처를 입력해 주세요!');
        resolve(false);
      }
      resolve(true);
    });
  }

  requestIMP() {
    this._is_loading = true;

    let now = new Date();
    let vbank_due = this.due_datetime_to_string();
    let m_uid = 'spc_' + now.getTime().toString();
    this._info.vbank_due = vbank_due;
    this._info.merchant_uid = m_uid;

    IMP.init('imp31268121');
    return new Promise((resolve, reject) => {
      IMP.request_pay(this._info, (rsp) => {
        if (!rsp.success) {
          let msg = '결제에 실패하였습니다.\r\n';
          msg += '에러내용 : ' + rsp.error_msg;
          alert(msg);
          resolve(false);
        } else {
          alert('결제요청에 성공하였습니다.\r\n결제요청정보를 저장합니다.\r\n페이지가 전환될 때까지 잠시 기다려주시기 바랍니다.');
          resolve(rsp);
        }
      });
    });
  }

  postTargetSerial(serial: any): Observable<object> {
    return this.$http.post('/api/payment/regist/serial', serial)
      .map((res: Response) => res.json())
      .catch(handleError);
  }

  postTargetAmount() {
    return this.$http.post('/api/payment/regist/' + this._info.custom_data.pkey_id + '/amount', { amount: this._info.amount })
      .map((res: Response) => res.json())
      .catch(handleError);
  }

  postOrderHistory(imp_res) {
    return this.$http.post('/api/payment/imp/pc/complete', imp_res)
      .map((res: Response) => res.json())
      .catch(handleError);
  }

  due_datetime_to_string() {
    let now = new Date();
    let str = '';

    str += now.getFullYear().toString();
    if (now.getMonth() + 1 < 10) {
      str += '0';
    }
    str += (now.getMonth() + 1).toString();
    if (now.getDate() + 1 < 10) {
      str += '0';
    }
    str += (now.getDate() + 1).toString();
    if (now.getHours() < 10) {
      str += '0';
    }
    str += now.getHours().toString();
    if (now.getMinutes() < 10) {
      str += '0';
    }
    str += now.getMinutes().toString();
    return str;
  }
}
