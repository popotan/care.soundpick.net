import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { handleError } from '../../../async-handling.observable';

import { SerialService } from '../../serial.service';

@Injectable()
export class CareService{

  constructor(private $http: Http,
              private serialService: SerialService) { }

  // registReceipt(){
  //   console.log(this.serialService._pkey);
  //   return this.$http.post('/api/care', this.serialService._pkey)
  //           .map((res:Response) => res.json()).catch(handleError);
  // }
}
