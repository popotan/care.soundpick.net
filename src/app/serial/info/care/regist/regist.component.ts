import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from '../../../../global.service';
import { PgService } from '../pg.service';
import { PkeyService } from '../../pkey.service';

@Component({
  selector: 'app-regist',
  templateUrl: './regist.component.html',
  styleUrls: ['./regist.component.css']
})
export class RegistComponent implements OnInit {

  constructor(
    public global: GlobalService,
    public pgService: PgService,
    public target: PkeyService,
    private router: Router
  ) { }

  ngOnInit() {
    this.pgService._info.custom_data.pkey_id = this.target.pkey.pkey_id;
  }

  is_request: boolean = false;
  is_amount: boolean = false;

  is_amount_posted($event) {
    this.is_amount = $event;
  }

  requestIMP() {
    this.is_request = true;
    this.pgService.validInfo().then(
      result => {
        if (result) {
          this.pgService.postTargetAmount()
            .subscribe(result => {
              if (result['message'] == 'SUCCESS') {
                this.pgService._info.custom_data.care_order_id = result['response'];
                this.pgService.requestIMP().then(
                  result2 => {
                    this.pgService.postOrderHistory(result2)
                      .subscribe(
                      result => {
                        if (result['status'] == 201) {
                          this.router.navigateByUrl('/payment/result/' + result['response']['resp_id'].toString());
                        } else {
                          this.router.navigateByUrl('/payment/result/fail');
                        }
                      },
                      error => {
                        alert('결제 결과 저장 중 문제가 발생하였습니다.\r\n070-7814-0007로 연락주시기 바랍니다.');
                      }
                      )
                  }
                );
              } else {
                alert('부정거래 방지를 위한 초기정보 전송에 실패하였습니다.');
              }
            }, error => {
              alert('부정거래 방지를 위한 초기정보 전송에 실패하였습니다.');
            });
        }
      }
    );
  }
}