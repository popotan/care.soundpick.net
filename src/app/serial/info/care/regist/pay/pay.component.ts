import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { PgService } from '../../pg.service';

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.css']
})
export class PayComponent implements OnInit {

  constructor(public pgService: PgService) { }

  ngOnInit() {
  }

  @Output() isValid = new EventEmitter<number>();
}
