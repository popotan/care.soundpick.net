import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { PgService } from '../../pg.service';

@Component({
  selector: 'app-clause',
  templateUrl: './clause.component.html',
  styleUrls: ['./clause.component.css']
})
export class ClauseComponent implements OnInit {

  constructor(public pgService: PgService, private router:Router) { }

  ngOnInit() {
  }

  @Output() isValid = new EventEmitter<number>();
}