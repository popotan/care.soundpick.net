import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { PkeyService } from '../../../pkey.service';
import { PgService } from '../../pg.service';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit {

  constructor(
    public target: PkeyService,
    public pgService: PgService, 
    private router:Router) { }

  ngOnInit() {
  }

  @Output() isAmount = new EventEmitter<boolean>();

  emitSelectInfo(str, amount){
    this.pgService._info.name = str;
    this.pgService._info.amount = parseInt(amount);
    this.isAmount.emit(true);
  }
}
