import { Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../../shared/shared.module';

import { PgService } from '../pg.service';
import { CareService } from '../care.service';

import { RegistComponent } from './regist.component';
import { SelectComponent } from './select/select.component';
import { PayComponent } from './pay/pay.component';
import { ClauseComponent } from './clause/clause.component';

import { InfoResolveService } from '../../info-resolve.service';
import { IsMatchedUidGuard } from '../../../../is-matched-uid.guard';
import { IsLoggedInGuard } from '../../../../is-logged-in.guard';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: RegistComponent,
      canActivate: [IsMatchedUidGuard],
      data : { 
        title : '케어서비스 구매',
        meta : [
          {property : 'og:title', content : '케어서비스 구매'},
          {property : 'og:description', content : '사운드픽케어가 제공하는 보증서비스를 구매할 수 있습니다.'},
          {name :'description', content : '사운드픽케어가 제공하는 보증서비스를 구매할 수 있습니다.'}
        ]
      },
      resolve: {
        target: InfoResolveService
      }
    },{
      path : '**',
      redirectTo : ''
    }])
  ],
  declarations: [SelectComponent, PayComponent, ClauseComponent, RegistComponent],
  providers: [PgService, IsLoggedInGuard, IsMatchedUidGuard]
})
export class RegistModule { }
