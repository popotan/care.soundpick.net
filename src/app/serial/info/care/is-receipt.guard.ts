import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { Location } from '@angular/common';

import { PkeyService } from '../pkey.service';

@Injectable()
export class IsReceiptGuard implements CanActivate, CanActivateChild {
  constructor(private target: PkeyService,
              private location: Location){ }

  canActivate(next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean {
      return this.canActivateChild(next, state);
    }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      console.log('isReceiptGuard 진입함.');
      if(this.target.pkey.care.receipt_filename){
        return true;
      }else{
        alert("구매영수증을 먼저 등록하셔야 합니다. 이전페이지로 돌아갑니다.");
        this.location.back();
        return false;
      }
  }
}
