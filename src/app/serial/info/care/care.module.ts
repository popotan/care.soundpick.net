import { Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';

import { CareService } from './care.service';
import { PgService } from './pg.service';
import { IsMatchedUidGuard } from '../../../is-matched-uid.guard';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([{
      path : 'regist',
      loadChildren : 'app/serial/info/care/regist/regist.module#RegistModule'
    },{
      path : 'call',
      loadChildren : 'app/serial/info/care/call/call.module#CallModule'
    },{
      path : '**',
      redirectTo : 'regist'
    }])
  ],
  declarations: [],
  providers : [CareService, PgService, IsMatchedUidGuard]
})
export class CareModule { }
