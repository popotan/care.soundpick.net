import { Component, OnInit } from '@angular/core';
import { PkeyService } from './pkey.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  constructor(public target: PkeyService) { }
  ngOnInit() {
  }
}
