import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { handleError } from '../../async-handling.observable';

import { Pkey } from '../pkey';

@Injectable()
export class PkeyService {

  /**
   *
   *
   * @type {Pkey}
   * @memberof PkeyService
   */
  pkey: Pkey;
  constructor(private $http: Http,
    private route: ActivatedRoute) { }

  /**
   *
   *
   * @param {*} pkey_id
   * @returns {Promise<Pkey>}
   * @memberof PkeyService
   */
  get(pkey_id): Promise<Pkey> {
    return new Promise((resolve, reject) => {
      this.$http.get('/api/serial/' + pkey_id)
        .map((res: Response) => res.json()).catch(handleError)
        .subscribe(
          result => {
            let _pkey = new Pkey();
            _pkey.pkey_id = result['response']['pkey_id'];
            _pkey.category = result['response']['category'];
            _pkey.aver_min = result['response']['aver_min'];
            _pkey.aver_max = result['response']['aver_max'];
            _pkey.unique_serial = result['response']['unique_serial'];
            _pkey.validation_number = result['response']['validation_number'];

            _pkey.product.brand = result['response']['product']['brand'];
            _pkey.product.model = result['response']['product']['model'];
            _pkey.product.name = result['response']['product']['name'];
            _pkey.product.thumbnail_url = result['response']['product']['thumbnail_url'];

            if (result['response']['care']) {
              _pkey.care.care_id = result['response']['care']['care_id'];
              _pkey.care.receipt_filename = result['response']['care']['receipt_filename'];
              _pkey.care.wrty_type = result['response']['care']['wrty_type'];
              if (result['response']['care']['wrty_len']) {
                _pkey.care.wrty_len = result['response']['care']['wrty_len'];
              }
              if (result['response']['care']['care_order']) {
                _pkey.care.care_order = result['response']['care']['care_order'];
              }
            }
            resolve(_pkey);
          }, error => {
            reject(null);
          });
    });
  }
}