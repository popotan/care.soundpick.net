import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Route } from '@angular/router';
import { SharedModule } from './shared/shared.module';

import { SerialService } from './serial.service';

import { ValidComponent } from './valid/valid.component';
import { ListComponent } from './list/list.component';
import { ProductComponent } from './list/product/product.component';

import { IsLoggedInGuard } from '../is-logged-in.guard';
import { IsAnonymousUserSessionGuard } from '../is-anonymous-user-session.guard';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([
      {
        path : 'list',
        component : ListComponent,
        canActivate : [IsLoggedInGuard,IsAnonymousUserSessionGuard],
        data : { 
          title : '내 제품 목록',
          meta : [
            {property : 'og:title', content : '내 제품 목록'},
            {property : 'og:description', content : '내가 보유하고 있는 SOUNDPick 제품을 보실 수 있습니다.'},
            {name : 'description', content : '내가 보유하고 있는 SOUNDPick 제품을 보실 수 있습니다.'}
            ]
        }
      },{
        path : 'valid',
        component : ValidComponent,
        data : { 
          title : '제품번호 확인',
          meta : [
            {property : 'og:title', content : '제품번호 확인'},
            {property : 'og:description', content : '제품의 정품여부를 확인할 수 있습니다.'},
            {name : 'description', content : '제품의 정품여부를 확인할 수 있습니다.'}
            ]
        }
      },{
        path : 'info',
        loadChildren : 'app/serial/info/info.module#InfoModule'
      },{
        path : '**',
        redirectTo : 'valid'
      }
    ])
  ],
  declarations: [
    ValidComponent, 
    ListComponent, ProductComponent
    ],
    providers : [SerialService]
})
export class SerialModule { }
