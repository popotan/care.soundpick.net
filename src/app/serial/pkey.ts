import { Injectable } from '@angular/core';

@Injectable()
export class Pkey{
  pkey_id: number;
  category: string;
  aver_min: string;
  aver_max: string;
  unique_serial: string;
  validation_number: string;
  care: Care;
  product: ProdInfo;

  constructor(){
    this.care = new Care();
    this.product = new ProdInfo();
  }
}

class CareOrder{
  order_id: number;
  code: number;
  message: string;
  reg_date: Date;

  imp_order_resp: [any];
}

class Care{
  care_id: number;
  receipt_filename: string = '';
  wrty_type: string;
  wrty_len: WarrantyLen;
  reg_date: Date;

  care_order: [CareOrder];

  constructor(){
    this.wrty_len = new WarrantyLen();
  }
}

class ProdInfo{
  prod_id: number;
  name: string;
  brand: string;
  model: string;
  thumbnail_url: string;
}

class WarrantyLen{
  s_date: Date;
  e_date: Date;
}