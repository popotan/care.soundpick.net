import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { FileUploadService } from './file-upload.service';
import { ImageUploaderDirective } from './image-uploader.directive';
import { PkeyDirective } from './pkey.directive';
import { PyDateDirective } from '../../py-date.directive';
import { CareTypeDirective } from '../../care-type.directive';

import { ReceiptUploaderComponent } from './receipt-uploader/receipt-uploader.component';
import { SerialValidatorComponent } from './serial-validator/serial-validator.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    ReceiptUploaderComponent,
    SerialValidatorComponent,
    ImageUploaderDirective,
    PkeyDirective,
    PyDateDirective,
    CareTypeDirective
  ],
  exports : [
    ReceiptUploaderComponent,
    SerialValidatorComponent,
    ImageUploaderDirective,
    PkeyDirective,
    PyDateDirective,
    CareTypeDirective
  ]
})
export class SharedModule { }
