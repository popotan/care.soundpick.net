import { Injectable, Optional } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { handleError } from '../../../async-handling.observable';

@Injectable()
export class SerialValidService {

  constructor(private $http: Http) { }

  get_pkey_id(string_serial){
    return this.$http.get('/api/serial/id', {params : {serial : string_serial}})
    .map((res: Response) => res.json()).catch(handleError)
  }
}
