import { TestBed, inject } from '@angular/core/testing';

import { SerialValidService } from './serial-valid.service';

describe('SerialValidService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SerialValidService]
    });
  });

  it('should ...', inject([SerialValidService], (service: SerialValidService) => {
    expect(service).toBeTruthy();
  }));
});
