import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SerialValidatorComponent } from './serial-validator.component';

describe('SerialValidatorComponent', () => {
  let component: SerialValidatorComponent;
  let fixture: ComponentFixture<SerialValidatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SerialValidatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SerialValidatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
