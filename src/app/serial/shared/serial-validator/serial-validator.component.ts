import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { SerialValidService } from './serial-valid.service';

@Component({
  selector: 'app-serial-validator',
  templateUrl: './serial-validator.component.html',
  styleUrls: ['./serial-validator.component.css'],
  providers : [SerialValidService]
})
export class SerialValidatorComponent implements OnInit {

  constructor(public serialValidService: SerialValidService) { }

  ngOnInit() {
  }

  @Output() receivePkeyId: EventEmitter<any> = new EventEmitter();

  _string_serial: string = '';

  _is_loading = false;
  _is_posted = false;
  _is_valid = false;
  _is_fully_typed = false;

  valid(){
    this._is_loading = true;
    this.serialValidService.get_pkey_id(this._string_serial.toUpperCase())
    .subscribe(result => {
      this._is_loading = false;
      if (result.status == 200){
        this.receivePkeyId.emit(result.response.pkey_id);
        this._is_posted = true;
        this._is_valid = true;
      }else{
        this._is_posted = true;
        this._is_valid = false;
      }
    });
  }

  retype = function(){
    this._string_serial = '';
    this._is_posted = false;
  }

  init = function(){
    this._string_serial = '';
    this._is_posted = false;
    this._is_valid = false;
  }

  isFullyTyped($event){
    console.log($event);
    this._is_fully_typed = $event;
  }
}
