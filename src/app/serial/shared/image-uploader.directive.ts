import { Directive, HostListener, Input, Output, EventEmitter } from '@angular/core';
import { FileUploadService } from './file-upload.service';

@Directive({
  selector: '[imageuploader]',
  providers : [FileUploadService]
})
export class ImageUploaderDirective {

  constructor(public fileUploadService : FileUploadService) { }

  @Input() imageuploader: any;
  @Output() uploadedFilename : EventEmitter<any> = new EventEmitter();

  @HostListener('change', ['$event']) onchange($event){
    this.fileUploadService.uploadFile($event.target.files)
    .then(
      result => {
        if(result.status == 201){
          switch(result.message){
            case 'SUCCESS':
            this.uploadedFilename.emit(result.response);
            break;
            case 'DUPLICATED':
            alert('이미 등록된 이미지입니다.');
            break;
            case 'FAIL':
            alert('파일전송에 실패하였습니다. 다시 시도해주세요!');
            break;
          }
        }
      },
      error => {
        alert('비정상적인 전송이 발생하였습니다. 다시 시도해주세요!');
      }
    )
  }
}
