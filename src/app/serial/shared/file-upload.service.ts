import { Injectable } from '@angular/core';

@Injectable()
export class FileUploadService {

  constructor() { }

  upload_progress:number = 0;
  upload_progress_observer:any;

  uploadFile(fileList:FileList): Promise<any>{
    let p = new Promise((resolve, reject) => {
      let file: File = fileList[0];
      let formData:FormData = new FormData();
      formData.append('upload_file', file, file.name);

      let xhr: XMLHttpRequest = new XMLHttpRequest();
      xhr.onreadystatechange = () => {
        if(xhr.readyState === 4){
          if(xhr.status === 200){
            resolve(JSON.parse(xhr.response));
          }else{
            reject(xhr.response);
          }
        }
      };

      xhr.upload.onprogress = (event) => {
        this.upload_progress = Math.round(event.loaded / event.total * 100);
      };

      xhr.open('POST', '/api/care/receipt', true);
      xhr.send(formData);
    });

    return p;
  }
}
