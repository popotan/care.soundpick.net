import { Directive, HostListener, Input, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[pkey]'
})
export class PkeyDirective {

  constructor() { }

  _value: string = '';
  _pre_value: string = '';

  @Output() ngModelChange : EventEmitter<any> = new EventEmitter();
  @Output() isFullTyped : EventEmitter<any> = new EventEmitter();

  @HostListener('keyup', ['$event'])
  onInputChange2($event){
    this._pre_value = this._value;
    
    if($event.target.value == null){
      this.ngModelChange.emit($event.target.value);
      this._value = $event.target.value;
      this.isFullTyped.emit(false);
    }else{
      let input_text = $event.target.value.toUpperCase();
      if(this.valid_regex(input_text)){
        this._value = this.insert_dash(input_text);
      }else{
        alert('한글, 특수문자 등 허용되지 않는 문자열이 있습니다.');
        this._value = this._pre_value;
      }
      $event.target.value = this._value;
      this.ngModelChange.emit(this._value);
      if(this._value.length > 18){
        this.isFullTyped.emit(true);
      }else{
        this.isFullTyped.emit(false);
      }
    }
  }

  valid_regex(text:string): boolean{
    if(text == ''){
      return true;
    }else{
      let regex = /^[a-zA-Z0-9\-]*$/;
      return regex.test(text);
    }
  }

  insert_dash(text:string): string{
    let text_arr : string[] = text.replace('-', '').split('');
    if (text_arr.length > 4 && text_arr[4] != '-'){
      text_arr.splice(4,0,'-');
    }
    if (text_arr.length > 9 && text_arr[9] != '-'){
      text_arr.splice(9,0,'-');
    }
    if (text_arr.length > 14 && text_arr[14] != '-'){
      text_arr.splice(14,0,'-');
    }
    if (text_arr.length > 19){
      alert('입력길이를 초과하였습니다.');
      text_arr = text_arr.slice(0, 18);
    }
    return text_arr.join('');
  }
}
