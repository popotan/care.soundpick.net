import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptUploaderComponent } from './receipt-uploader.component';

describe('ReceiptUploaderComponent', () => {
  let component: ReceiptUploaderComponent;
  let fixture: ComponentFixture<ReceiptUploaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiptUploaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptUploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
