import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { SerialService } from '../../serial.service';

@Component({
  selector: 'app-receipt-uploader',
  templateUrl: './receipt-uploader.component.html',
  styleUrls: ['./receipt-uploader.component.css'],
  inputs: ['pkeyid']
})
export class ReceiptUploaderComponent implements OnInit {

  /**
   *
   *
   * @memberof ReceiptUploaderComponent
   */
  @Output() uploadedReceipt = new EventEmitter<any>();

  /**
   *
   *
   * @type {number}
   * @memberof ReceiptUploaderComponent
   */
  pkeyid: number;

  constructor(public serialService: SerialService) { }

  ngOnInit() {
  }

  /**
   *
   *
   * @param {*} $event
   * @memberof ReceiptUploaderComponent
   */
  updateCareInfo($event) {
    this.serialService.postReceipt($event, this.pkeyid)
      .subscribe(result => {
        this.uploadedReceipt.emit(result);
      }, error => console.error(error));
  }
}