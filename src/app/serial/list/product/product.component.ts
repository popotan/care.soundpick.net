import { Component, OnInit } from '@angular/core';

import { Pkey } from '../../pkey';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs: ['pkey']
})
export class ProductComponent implements OnInit {
  /**
   *
   *
   * @type {Pkey}
   * @memberof ProductComponent
   */
  pkey: Pkey;

  /**
   *
   *
   * @type {boolean}
   * @memberof ProductComponent
   */
  _image_uploader = false;
  
  constructor() { }

  ngOnInit() {
  }
}
