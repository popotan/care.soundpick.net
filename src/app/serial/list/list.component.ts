import { Component, OnInit } from '@angular/core';

import { GlobalService } from '../../global.service';
import { SerialService } from '../serial.service';
import { PkeyService } from '../info/pkey.service';
import { Pkey } from '../pkey';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [PkeyService]
})
export class ListComponent implements OnInit {

  /**
   *
   *
   * @type {Array<Pkey>}
   * @memberof ListComponent
   */
  _list: Array<Pkey>;

  constructor(
    public global: GlobalService,
    private serialService: SerialService,
    public pkeyService: PkeyService) { }

  ngOnInit() {
    this._list = [];
    this.get_list();
  }

  /**
   *
   *
   * @memberof ListComponent
   */
  get_list() {
    this.global.is_loading = true;
    this.serialService.get_list()
      .subscribe(
        result => {
          result['response'].forEach(pkey_id => {
            this.pkeyService.get(pkey_id).then(
              result2 => this._list.push(result2)
            );
          });
        },
        error => {
          console.error(error);
        },
        () => {
          this.global.is_loading = false;
        }
      );
  }
}
