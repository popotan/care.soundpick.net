import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { handleError } from '../async-handling.observable';

@Injectable()
export class SerialService {

  constructor(private $http: Http) { }

  get_list(){
    return this.$http.get('/api/serial')
    .map((res: Response) => res.json()).catch(handleError);
  }

  postReceipt(filename, pkey_id){
    return this.$http.post('/api/care', {filename : filename, pkey_id : pkey_id})
    .map((res: Response) => res.json()).catch(handleError);
  }
}