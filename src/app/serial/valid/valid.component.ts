import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SessionService } from '../../session.service';

@Component({
  selector: 'app-valid',
  templateUrl: './valid.component.html',
  styleUrls: ['./valid.component.css']
})
export class ValidComponent implements OnInit {

  /**
   *
   *
   * @type {boolean}
   * @memberof ValidComponent
   */
  _loginGuide:boolean = false;

  /**
   *
   *
   * @type {string}
   * @memberof ValidComponent
   */
  _redirect_url:string = '/login?r_url=';

  constructor(
    public session: SessionService,
    public router: Router) { }

  ngOnInit() {
  }

  /**
   *
   *
   * @param {number} pkey_id
   * @memberof ValidComponent
   */
  jumpTo(pkey_id: number){
    // this.router.navigateByUrl('/serial/info/'+pkey_id);
    if(this.session._is_logged_in){
      this.router.navigateByUrl('/serial/info/'+pkey_id);
    }else{
      this._redirect_url += encodeURIComponent('/serial/info/'+pkey_id);
      this._loginGuide = true;
    }
  }
}