import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { handleError } from './async-handling.observable';

import { GlobalService } from './global.service';

export class UserInfo{
  user_uid : number;
  name : string;
  profile_image: string;
  expires_in: Date;
  email : string;
  division : string|null|undefined;

  constructor(){
    this.user_uid = 0;
    this.name;
    this.profile_image;
    this.expires_in = new Date();
    this.email;
    this.division;
  }
}

@Injectable()
export class SessionService {

  constructor(
    private $http: Http,
    public global: GlobalService) { }
  _is_logged_in: boolean = false;
  _is_loading: boolean = false;
  _state: string;
  _info: UserInfo = new UserInfo();

  getSession(){
    this.global.is_loading = true;
    this._is_loading = true;
    return new Promise((resolve, reject) => {
      this.$http.get('/api/user/session')
      .map((res: Response) => res.json()).catch(handleError)
    .subscribe(data => {
      this._info = data.info ? data.info : new UserInfo();
      this._state = data.state;
      this._is_logged_in = data.is_logged_in;
      this._is_loading = false;
      this.global.is_loading = false;
      resolve(this._info);
    });
    });
  }

  getStatusToken(social:string){
    switch (social){
      case 'naver':
        return this.$http.get('/api/user/status_token/naver')
                    .map((res: Response) => res.json())
                    .catch(handleError);
      case 'facebook':
        return this.$http.get('/api/user/status_token/facebook')
                    .map((res: Response) => res.json())
                    .catch(handleError);
    }
  }
}
