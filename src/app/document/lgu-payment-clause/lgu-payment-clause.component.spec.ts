import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LguPaymentClauseComponent } from './lgu-payment-clause.component';

describe('LguPaymentClauseComponent', () => {
  let component: LguPaymentClauseComponent;
  let fixture: ComponentFixture<LguPaymentClauseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LguPaymentClauseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LguPaymentClauseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
