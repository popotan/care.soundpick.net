import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { IntroServiceComponent } from './intro-service/intro-service.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { QnaComponent } from './qna/qna.component';
import { ReceiptComponent } from './receipt/receipt.component';
import { FindPkeyComponent } from './find-pkey/find-pkey.component';
import { ClauseComponent } from './clause/clause.component';
import { LguPaymentClauseComponent } from './lgu-payment-clause/lgu-payment-clause.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path : '',
      component : IntroServiceComponent
    },{
      path : 'clause',
      component : ClauseComponent,
      data : { 
        title : '이용약관',
        meta : [
          {property : 'og:title', content : '사운드픽케어 이용약관'},
          {property : 'og:description', content : '사운드픽케어의 이용약관을 확인하실 수 있습니다.'},
          {name : 'description', content : '사운드픽케어의 이용약관을 확인하실 수 있습니다.'}
        ]
      }
    },{
      path : 'intro_service',
      component : IntroServiceComponent
    },{
      path : 'privacy_policy',
      component : PrivacyPolicyComponent,
      data : { 
        title : '개인정보보호방침',
        meta : [
          {property : 'og:title', content : '사운드픽케어 개인정보보호방침'},
          {property : 'og:description', content : '사운드픽케어의 개인정보보호방침을 확인하실 수 있습니다.'},
          {name : 'description', content : '사운드픽케어의 개인정보보호방침을 확인하실 수 있습니다.'}
        ]  
      }
    },{
      path : 'qna',
      component : QnaComponent,
      data : { 
        title : '고객문의',
        meta : [
          {property : 'og:title', content : '사운드픽케어 고객문의'},
          {property : 'og:description', content : '사운드픽케어에 문의할 수 있는 방법을 안내해 드립니다.'},
          {name : 'description', content : '사운드픽케어에 문의할 수 있는 방법을 안내해 드립니다.'}
        ]
      }
    },{
      path : 'receipt',
      component : ReceiptComponent
    },{
      path : 'findpkey',
      component : FindPkeyComponent
    },{
      path : 'lgu-payment-clause',
      component : LguPaymentClauseComponent
    }])
  ],
  declarations: [IntroServiceComponent, PrivacyPolicyComponent, QnaComponent, ReceiptComponent, FindPkeyComponent, ClauseComponent, LguPaymentClauseComponent]
})
export class DocumentModule { }
