import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindPkeyComponent } from './find-pkey.component';

describe('FindPkeyComponent', () => {
  let component: FindPkeyComponent;
  let fixture: ComponentFixture<FindPkeyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindPkeyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindPkeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
