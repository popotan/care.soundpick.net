import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroServiceComponent } from './intro-service.component';

describe('IntroServiceComponent', () => {
  let component: IntroServiceComponent;
  let fixture: ComponentFixture<IntroServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
