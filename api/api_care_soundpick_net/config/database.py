# MYSQL
SQLALCHEMY_TRACK_MODIFICATIONS = True

# MySQL
# Uncomment the below string if you are using MySQL or MariaDB
SQLALCHEMY_DATABASE_URI = "mysql+pymysql://root:Funclass07@localhost/care_service?charset=utf8&use_unicode=0"
SQLALCHEMY_BINDS = {
    'sp_admin_v2' : "mysql+pymysql://root:Funclass07@localhost/sp_admin_v2?charset=utf8&use_unicode=0",
    'care_service': "mysql+pymysql://root:Funclass07@localhost/care_service?charset=utf8&use_unicode=0"
}