from flask import Blueprint, render_template, jsonify, request, redirect, session

from api_care_soundpick_net.composer.imp_payment import ImpPayment
from api_care_soundpick_net.composer.CareOrderDatabaseOrganizer import CareOrderDatabaseOrganizer

from api_care_soundpick_net.model.pkey import pkey_orm
from api_care_soundpick_net.model.imp_order_resp import imp_order_resp_orm
from api_care_soundpick_net.model.imp_order_resp_cncl import imp_order_resp_cncl_orm

from api_care_soundpick_net.composer.uid_validator import valid_uid

import json

payment = Blueprint('payment', __name__)

@payment.route('/regist/<int:pkey_id>/amount', methods=['POST'])
def target_amount_add(pkey_id):
    _cod = CareOrderDatabaseOrganizer().set_by_pkey_uid(pkey_id)
    _cod.set_amount(request.get_json()['amount'])
    return jsonify({
        'status' : 201,
        'message' : 'SUCCESS',
        'response' : _cod.pkey.care[0].care_order[-1].order_id
    }), 200

@payment.route('/regist/serial', methods=['POST'])
def target_serial_add():
    req = request.get_json()
    category = req['category']
    aver_min = req['aver_min']
    aver_max = req['aver_max']
    unique_serial = req['unique_serial']
    validation_number = req['validation_number']
    pkey = pkey_orm().query.filter_by(category=category)\
        .filter_by(aver_min=aver_min)\
        .filter_by(aver_max=aver_max)\
        .filter_by(unique_serial=unique_serial)\
        .filter_by(validation_number=validation_number)\
        .first()
    if pkey:
        session['pkey_id'] = pkey.pkey_id
        return jsonify({
            'target_serial' : pkey.category + '-' + pkey.aver_min + '-' + pkey.aver_max + '-' + pkey.unique_serial + pkey.validation_number
            })
    else:
        session['pkey_id'] = None
        return ('serial is not validated.', 401)

@payment.route('/imp/pc/complete', methods=['POST'])
def imp_order_complete_on_pc():
    imp_uid = request.get_json()['imp_uid']
    _rpi = receive_payment_info(imp_uid)
    if _rpi:
        return jsonify({
            'status' : 201,
            'message' : 'SUCCESS',
            'response' : {
                'resp_id' : _rpi.resp_id
            }
        })
    else:
        return jsonify({
            'status' : 204,
            'message' : 'FAIL'
        })

@payment.route('/imp/mobile/complete', methods=['GET'])
def imp_order_complete_on_mobile():
    if request.args.get('imp_success') == 'true':
        imp_uid = request.args.get('imp_uid')
        _rpi = receive_payment_info(imp_uid)
        if _rpi:
            return redirect('/payment/result/' + str(_rpi.resp_id), code=302)
        else:
            return redirect('/payment/result/fail', code=302)

def receive_payment_info(imp_uid):
    print('received_payment_info')
    ip = ImpPayment(imp_uid=imp_uid)
    payment_result = ip.getPaymentResult()._result
    
    if payment_result["code"] == 0 and payment_result["response"]["pg_tid"]:
        cdata = json.loads(payment_result["response"]["custom_data"])
        _cod = CareOrderDatabaseOrganizer()\
                .set_by_pkey_uid(cdata["pkey_id"])
        
        if payment_result["response"]["amount"] == _cod.get_amount(care_order_id=cdata['care_order_id']):
            #custom_data json parse
            if 'division' in session:
                user_div = session['division']
            else:
                user_div = cdata['user_div']
            if 'user_uid' in session:
                user_uid = session['user_uid']
            else:
                user_uid = cdata['user_uid']
            _cod.set_info(care_order_id=cdata["care_order_id"],\
                            payment_info=payment_result,\
                            user_div=user_div,\
                            user_uid=user_uid)

            _cod.regist_order_hist('SUCCESS')
            return _cod.pkey.care[0].care_order[-1].imp_order_resp[-1]
        else:
            return False
    else:
        return False

@payment.route('/imp/resp/<int:resp_id>', methods=['GET'])
def get_imp_resp(resp_id):
    ior = imp_order_resp_orm().query.filter_by(resp_id=resp_id).first()
    if ior:
        _repr = ["resp_id", "merchant_uid", "pg_tid", "pay_method", "name", "receipt_url", "paid_at", "status", "buyer_email", "amount", "apply_num"]
        _resp = {_col : getattr(ior, _col) for _col in _repr}

        return jsonify({
            "status" : 200,
            "message" : "SUCCESS",
            "response" : _resp
        }), 200
    else:
        return jsonify({
            "status" : 204,
            "message" : "PAYMENT_RESPONSE_IS_NOT_EXIST"
        }), 200