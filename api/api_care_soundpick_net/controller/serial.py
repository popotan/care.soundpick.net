from flask import Blueprint, render_template, jsonify, request, session

from api_care_soundpick_net.composer.PkeyDatabaseOrganizer import PkeyDatabaseOrganizer
from api_care_soundpick_net.composer.CareDatabaseOrganizer import CareDatabaseOrganizer

from api_care_soundpick_net.model.care import care_orm

from api_care_soundpick_net.composer.uid_validator import valid_uid

serial = Blueprint('serial', __name__)

@serial.route('/id', methods=['GET'])
def is_serial_exist():
    try:
        serial = request.args.get('serial')
        _p = PkeyDatabaseOrganizer().set_by_pkey_str(serial)

        _repr = ['pkey_id', 'category', 'aver_min', 'aver_max', 'unique_serial', 'validation_number']
        _resp = {_col : getattr(_p.pkey, _col) for _col in _repr}

        return jsonify({
            'status' : 200,
            'message' : 'SUCCESS',
            'response' : _resp
        }), 200
    except Exception as e:
        return jsonify({
            'status' : 204,
            'message' : str(e)
        }), 200

@serial.route('/<int:pkey_id>', methods=['GET'])
def get_serial_info_by_pkey_id(pkey_id):
    try:
        _p = CareDatabaseOrganizer().set_by_pkey_uid(pkey_id).get_prod_info()
        _resp = pkey_info_responsor(_p)
        return jsonify({
            'status' : 200,
            'message' : 'SUCCESS',
            'response' : _resp
        }), 200
    except Exception as e:
        return jsonify({
            'status' : 204,
            'message' : str(e)
        }), 200

@serial.route('/<int:pkey_id>/qualification', methods=['POST'])
def get_qualification_to_pkey(pkey_id):
    try:
        _p = CareDatabaseOrganizer().set_by_pkey_uid(pkey_id)
        ### 테스트용 코드
        if _p.pkey.care[0].user_uid == 0:
            return jsonify({
                'status' : 201,
                'message' : 'QUALIFIED_FOR_TEST'
            }), 200
        ### 테스트용 코드 끝
        if 'user_uid' in session and session['user_uid'] != 0:
            if _p.pkey.care[0].user_uid == session['user_uid']:
                return jsonify({
                    'status' : 201,
                    'message' : 'QUALIFIED'
                }), 200
            else:
                return jsonify({
                    'status' : 201,
                    'message' : 'UNQUALIFIED'
                }), 200
        else:
            return jsonify({
                'status' : 201,
                'message' : 'USER_SESSION_IS_NOT_EXIST'
            }), 200
    except Exception as e:
        print(e)
        return jsonify({
            'status' : 400,
            'message' : str(e)
        }), 200

@serial.route('', methods=['GET'])
def my_serial_list():
    try:
        _cl = care_orm().query.filter_by(user_uid=session['user_uid']).all()

        _resp = [getattr(_c, 'pkey_id') for _c in _cl]
        
        return jsonify({
            'status' : 200,
            'message' : 'SUCCESS',
            'response' : _resp
        }), 200
    except Exception as e:
        return jsonify({
            'status' : 204,
            'message' : str(e)
        }), 200

def pkey_info_responsor(DatabaseOrganizerInstance):
    _repr_p = ['pkey_id', 'category', 'aver_min', 'aver_max', 'unique_serial', 'validation_number']
    _resp_p = {_col : getattr(DatabaseOrganizerInstance.pkey, _col) for _col in _repr_p}

    if hasattr(DatabaseOrganizerInstance, 'product'):
        _repr_pi = ['name', 'brand', 'model', 'thumbnail_url']
        _resp_p['product'] = {_col : getattr(DatabaseOrganizerInstance.product, _col) for _col in _repr_pi}
    else:
        _resp_p['product'] = None

    _resp_p['care'] = {}
    if len(DatabaseOrganizerInstance.pkey.care):
        _repr_c = ['care_id', 'receipt_filename', 'reg_date']
        _resp_p['care'] = {_col : getattr(DatabaseOrganizerInstance.pkey.care[0], _col) for _col in _repr_c}

        _resp_p['care']['wrty_len'] = None
        if len(DatabaseOrganizerInstance.pkey.care[0].care_wrty_hist):
            _repr_cw = ['s_date', 'e_date']
            _resp_p['care']['wrty_len'] = {_col : getattr(DatabaseOrganizerInstance.pkey.care[0].care_wrty_hist[-1], _col) for _col in _repr_cw}

        if len(DatabaseOrganizerInstance.pkey.care[0].care_order):
            _repr_co = ['order_id', 'code', 'message', 'reg_date']
            _resp_p['care']['care_order'] = [{_col: getattr(_row, _col) for _col in _repr_co} for _row in DatabaseOrganizerInstance.pkey.care[0].care_order]

            for _co_idx, _co_inst in enumerate(DatabaseOrganizerInstance.pkey.care[0].care_order):
                if len(_co_inst.imp_order_resp):
                    _repr_ior = ['resp_id', 'pay_method', 'status', 'pg_tid', 'reg_date']
                    _resp_p['care']['care_order'][_co_idx]['imp_order_resp'] = [{_col : getattr(_row, _col) for _col in _repr_ior} for _row in _co_inst.imp_order_resp]
            
    _resp_p['care']['wrty_type'] = DatabaseOrganizerInstance.get_wrty_state()
        
    return _resp_p
