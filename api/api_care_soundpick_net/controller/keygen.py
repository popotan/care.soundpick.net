from flask import Blueprint, render_template, jsonify, request
from api_care_soundpick_net.config import ALPHA_KEY_MAP

keygen = Blueprint('keygen', __name__)

@keygen.route('', methods=['POST'])
def is_key_exist():
    product_key = request.get_json()['pkey']
    return jsonify({'is_key_exist' : False})

def validate_key(pkey):
    if isinstance(pkey, str):
        pkey = pkey.upper()
        pkey_str_arr = pkey.split('')
        stack = []
        MIN_CARE_PRICE = 0
        MAX_CARE_PRICE = 0
        validate_number = 0

        for idx, char in enumerate(pkey_str_arr[:3]):
            stack.append(ALPHA_KEY_MAP[char] * (idx + 1))

        for n in stack:
            validate_key = validate_key + n
            #compare validate_number with pkey of last place throw if state

        for char in pkey_str_arr[4:7]:
            MIN_CARE_PRICE = MIN_CARE_PRICE + ALPHA_KEY_MAP[char]

        for char in pkey_str_arr[8:11]:
            MAX_CARE_PRICE = MAX_CARE_PRICE + ALPHA_KEY_MAP[char]

        
    else:
        raise