from flask import Blueprint, render_template, jsonify, request, session

from api_care_soundpick_net.composer.CareDatabaseOrganizer import CareDatabaseOrganizer
from api_care_soundpick_net.composer.CareCallDatabaseOrganizer import CareCallDatabaseOrganizer

from api_care_soundpick_net.composer.sms import Sms
from api_care_soundpick_net.config import static as STATIC

from api_care_soundpick_net.model.care_call import care_call_orm
from api_care_soundpick_net.model.care_call_hist import care_call_hist_orm

from api_care_soundpick_net.composer.uid_validator import valid_uid

from datetime import datetime, timedelta

care = Blueprint('care', __name__)

@care.route('', methods=['POST'])
def care_info():
    try:
        pkey_id = request.get_json()['pkey_id']
        filename = request.get_json()['filename']
        _c = CareDatabaseOrganizer().set_by_pkey_uid(pkey_id)\
            .add(receipt_filename=filename, user_div=session['division'], user_uid=session['user_uid'])

        _repr = ['care_id', 'receipt_filename', 'reg_date']
        _resp = {_col : getattr(_c.pkey.care[0], _col) for _col in _repr}

        return jsonify({
            'status' : 201,
            'message' : 'SUCCESS',
            'response' : _resp
        }), 200
    except Exception as e:
        return jsonify({
            'status' : 204,
            'message' : str(e)
        }), 200

@care.route('/change')
def care_upgrade():
    return jsonify(result)

@care.route('/cancel')
def care_cancel():
    return jsonify(result)

@care.route('/call/<int:pkey_id>', methods=['GET', 'POST'])
def care_call(pkey_id):
    try:
        _cc = CareCallDatabaseOrganizer().set_by_pkey_uid(pkey_id)
        if request.method == 'GET':
            pass
        elif request.method == 'POST':
            _info = request.get_json()

            if _cc.is_wrty_valid:
                _new_cc = care_call_orm()
                _new_cc.reason = _info['reason']

                if not _info['user_addr_hist_id']: #user_addr.hist_id 가 없으면 생성하기
                    _new_addr = user_addr_hist_orm()
                    _new_addr.user_uid = session['user_uid']
                    _new_addr.postcode = _info['postcode']
                    _new_addr.province = _info['province']
                    _new_addr.avenue = _info['avenue']
                    _new_addr.phone1 = _info['phone1']
                    _new_addr.phone2 = _info['phone2']
                    _new_addr.add()
                    _new_cc.user_addr_hist_id = _new_addr.hist_id
                else:
                    _new_cc.user_addr_hist_id = _info['user_addr_hist_id']

                _cc.pkey.care[0].care_call.append(_new_cc)
                _cc.pkey.update()

                return jsonify({
                    'status' : 201,
                    'message' : 'SUCCESS'
                }), 200
            else:
                return jsonify({
                    'status' : 204,
                    'message' : 'WARRANTY_WAS_EXPIRED'
                })
    except Exception as e:
        return jsonify({
            'status' : 204,
            'message' : str(e)
        })
        
# @care.route('/call', methods=['GET', 'POST'])
# def care_call():
#     if request.method == 'GET':
#         call_history = care_call_hist_orm().query
#         if 'unique_user_id' in session:
#             call_history = call_history.filter_by(unique_user_id=session['unique_user_id'])
#         else:
#             call_history = call_history.filter(\
#             or_(care_call_hist_orm.phone1 == request.args.get('phone'), \
#             care_call_hist_orm.phone2 == request.args.get('phone')))
#         call_history = call_history.all()
#         repr_cols = ['call_id', 'pkey_id', 'postcode', 'province', 'avenue', 'phone1', 'phone2', 'reg_date']
#         result = [{ _col : getattr(_row, _col) for _col in repr_cols } for _row in call_history]
#         return jsonify(result)
#     elif request.method == 'POST':
#         call_history = care_call_hist_orm().query.filter_by(call_id=request.get_json()['call_id']).first()
#         call_history.postcode = request.get_json()['postcode']
#         call_history.province = request.get_json()['province']
#         call_history.avenue = request.get_json()['avenue']
#         call_history.phone1 = request.get_json()['phone1']
#         call_history.phone2 = request.get_json()['phone2']
#         call_history.reason = request.get_json()['reason']
#         call_history.update()

#         repr_cols = ['call_id', 'pkey_id', 'postcode', 'province', 'avenue', 'phone1', 'phone2', 'reg_date']
#         result = {_col : getattr(call_history, _col) for _col in repr_cols}
#         return jsonify(result)

@care.route('/receipt', methods=['POST'])
def post_recept():
    def upload():
        def allowed_file(filename):
	        ALLOWED_EXTENSIONS = set(['png', 'jpeg', 'jpg', 'gif', 'PNG', 'JPEG', 'JPG', 'GIF'])
	        return '.' in filename and \
			    filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS
        
        file = request.files['upload_file']
        if file and allowed_file(file.filename):
            now = datetime.utcnow() + timedelta(hours=9)
            
            filename = str(now.year) + str(now.month) + str(now.day) + str(now.hour) + str(now.minute) + str(now.second)
            filename += '_' + file.filename
            file.save(STATIC.FILE_PATH.format('care/receipt', filename))
            return filename
        else:
            return ''

    if request.method == 'POST':
        filename = upload()
        if filename:
            return jsonify({
                'status' : 201,
                'message' : 'SUCCESS',
                'response' : filename
            }), 200
        else:
            return jsonify({
                'status' : 204,
                'message' : 'FAIL'
            }), 200
    else:
        return ('', 404)