from flask import Blueprint, render_template, request, Request, jsonify, session, redirect
import json
import urllib
import os
from datetime import datetime, timedelta
import random

from flask_bcrypt import generate_password_hash

from api_care_soundpick_net import app
from api_care_soundpick_net.model.user import user_orm
from api_care_soundpick_net.model.user_addr_hist import user_addr_hist_orm

from api_care_soundpick_net.composer.facebook_login import FacebookLogin
from api_care_soundpick_net.composer.naver_login import NaverLogin
from api_care_soundpick_net.composer.sms import Sms

from api_care_soundpick_net.composer.uid_validator import valid_uid

SESSION_KEY_LIST = ['division', 'user_uid', 'name', 'email', 'profile_image', 'expires_in']

user = Blueprint('user', __name__)

@user.route('/session')
def get_session():
    if not 'division' in session:
        session['division'] = None
    if not 'user_uid' in session:
        session['user_uid'] = 0
    _data = {
        "state" : "empty",
        "info" : None,
        "is_logged_in" : False
    }
    if not is_expired_token():
        _data['state'] = None
        _data['is_logged_in'] = True
        _data['info'] = {}
        for key in SESSION_KEY_LIST:
            if key in session:
                _data['info'][key] = session[key]
    return jsonify(_data)

def is_expired_token():
    if 'expires_in' in session:
        expires_time = session['expires_in']
        now = datetime.utcnow()+timedelta(hours=9)
        if now >= expires_time:
            return True
        else: return False
    else:
        return True

@user.route('/valid/phone', methods=['POST'])
def set_valid_number():
    if request.method == 'POST':
        valid_number = random.randrange(100000,1000000)
        sms = Sms(
            to_number=request.get_json()['phone'],
            from_number='07078140007',
            desc='사운드픽 케어 인증번호는 [{}]입니다.'.format(valid_number)
            ).send()
        if sms.result:
            session['phone'] = request.get_json()['phone']
            session['valid_number'] = valid_number
            session['valid_expires_in'] = datetime.utcnow()+timedelta(hours=9)+timedelta(seconds=300)
            return jsonify({
                'status' : 201,
                'message' : 'SUCCESS'
            }), 200
        else:
            return jsonify({
                'status' : 401,
                'message' : 'FAIL_SEND_SMS'
            }), 200

@user.route('/session/anonymous', methods=['POST'])
def make_anonymous_session():
    if 'valid_expires_in' in session:
        if session['valid_expires_in'] < datetime.utcnow()+timedelta(hours=9):
            return jsonify({
                'status' : 204,
                'message' : 'VALID_TIME_WAS_EXPIRED'
            }), 200
    if 'valid_number' in session:
        if session['valid_number'] == int(request.get_json()['valid_number']):
            _user = user_orm().query.filter_by(phone_num=request.get_json()['phone']).first()
            if _user:
                _user.expires_in = datetime.utcnow()+timedelta(hours=9)+timedelta(hours=1)
                _user.update()
                _resp = {_col : getattr(_user, _col) for _col in SESSION_KEY_LIST}
                make_session(_user)
            else:
                _new_user = user_orm()
                _new_user.user_id = 'ANONYMOUS'
                _new_user.division = 'GUEST'
                _new_user.phone_num = request.get_json()['phone']
                _new_user.expires_in = datetime.utcnow()+timedelta(hours=9)+timedelta(hours=1)
                _new_user.add()
                _resp = {_col : getattr(_new_user, _col) for _col in SESSION_KEY_LIST}
                make_session(_new_user)

            return jsonify({
                    'status' : 201,
                    'message' : 'SUCCESS'
                }), 200
        else:
            return jsonify({
                'status' : 204,
                'message' : 'VALID_NUMBER_IS_NOT_MATCHED'
            }), 200
    else:
        return jsonify({
            'status' : 204,
            'message' : 'VALID_NUMBER_IS_NOT_EXIST'
        }), 200

@user.route('/session/destroy', methods=['GET'])
def logout():
    session.clear()
    #로그인한 division에 따라 처리방법 분기하기
    return redirect('/', code=302)

@user.route('/previous_url', methods=['POST'])
def set_previous_page():
    session['previous_url'] = request.get_json('url')
    return ('', 200)

@user.route('/status_token/<social>', methods=['GET'])
def get_status_token(social):
    if social == 'naver':
        nowTime = '{0:%Y-%m-%d %H:%M:%S}'.format(datetime.utcnow()+timedelta(hours=9))
        ip = request.access_route
        nonHashedToken = nowTime + " " + str(ip)
        hashedToken = generate_password_hash(nonHashedToken)
    elif social == 'facebook':
        nowTime = '{0:%Y-%m-%d %H:%M:%S}'.format(datetime.utcnow()+timedelta(hours=9))
        ip = request.access_route
        nonHashedToken = nowTime + " " + str(ip)
        hashedToken = generate_password_hash(nonHashedToken)
    session['status_token'] = hashedToken.decode('utf8')
    return jsonify({"token" : session['status_token']})
        
@user.route('/redirect_url', methods=['POST'])
def receive_redirect_url():
    if request.method == 'POST':
        session['redirect_url'] = request.get_json()['redirect_url']
        print(session['redirect_url'])
        return jsonify({
            'status' : 201,
            'message' : 'SUCCESS'
        }), 200
    else:
        return 400

@user.route('/login/<social>', methods=['GET'])
def social_login_steps(social):
    _msg = ''
    _code = 401
    if social == 'facebook':
        try:
            fb = FacebookLogin()
            fb.redirect_url = 'http://care.soundpick.net/api/user/login/facebook'
            fb.exchangeAccessToken(code=request.args.get('code'))\
                .debugAccessToken()\
                .getUserInfo()
            saved_info = save_user_info(fb)
            make_session(saved_info)
            return do_redirect()
        except Exception as identifier:
            _msg = '페이스북 로그인 도중 에러가 발생하였습니다.'
            _code = 401
            return (_msg, _code)
        else:
            _msg = '페이스북 로그인에 성공하였으나, 이전페이지로 리다이렉트에 실패하였습니다.'
            _code = 200
            return (_msg, _code)
    elif social == 'naver':
        try:
            nl = NaverLogin()
            nl.redirect_url = 'http://care.soundpick.net/api/user/login/naver'
            nl.exchangeAccessToken(state=request.args.get('state'), code=request.args.get('code'))\
            .getUserInfo()
            saved_info = save_user_info(nl)
            make_session(saved_info)
            return do_redirect()
        except Exception as i:
            _msg = '네이버 로그인 도중 에러가 발생하였습니다.'
            _code = 401
            return (_msg, _code)
        else:
            msg = '네이버 로그인에 성공하였으나, 이전페이지로 리다이렉트에 실패하였습니다.'
            _code = 200
            return (_msg, _code)
        pass

def do_redirect():
    if 'redirect_url' in session:
        r_url = session['redirect_url']
        session['redirect_url'] = ''
        if r_url != '/' and r_url != '' and r_url != 'undefined':
            return redirect('http://care.soundpick.net' + r_url, code=302)
    return redirect('/', code=302)

def save_user_info(info):
    if isinstance(info, FacebookLogin):
        _old_user = user_orm().query.filter_by(division='FACEBOOK')\
                    .filter_by(user_id=info._access_info._user_id).first()
        if not _old_user:
            _user = user_orm()
            _user.division = 'FACEBOOK'
            _user.user_id = info._access_info._user_id
            _user.access_token = info._access_token_info._access_token
            _user.expires_in = info.calcExpirationTime()
            _user.email = info._user_info._email
            _user.name = info._user_info._name
            if 'min' in info._user_info._age_range:
                _user.age = str(info._user_info._age_range['min'])
            _user.age += '-'
            if 'max' in info._user_info._age_range:
                _user.age += str(info._user_info._age_range['max'])
            _user.gender = info._user_info._gender
            _user.profile_image = info._user_info._profile_image_url
            _user.add()
            return _user
        else:
            _old_user.access_token = info._access_token_info._access_token
            _old_user.expires_in = info.calcExpirationTime()
            _old_user.name = info._user_info._name
            if 'min' in info._user_info._age_range:
                _old_user.age = str(info._user_info._age_range['min'])
            _old_user.age += '-'
            if 'max' in info._user_info._age_range:
                _old_user.age += str(info._user_info._age_range['max'])
            _old_user.gender = info._user_info._gender
            _old_user.profile_image = info._user_info._profile_image_url
            _old_user.update()
            return _old_user

    elif isinstance(info, NaverLogin):
        _old_user = user_orm().query.filter_by(division='NAVER')\
                    .filter_by(user_id=info._user_info._id).first()
        if not _old_user:
            _user = user_orm()
            _user.division = 'NAVER'
            _user.user_id = info._user_info._id
            _user.access_token = info._access_token_info._access_token
            _user.expires_in = info.calcExpirationTime()
            _user.email = info._user_info._email
            _user.name = info._user_info._name
            _user.age = info._user_info._age
            _user.gender = info._user_info._gender
            _user.refresh_token = info._access_token_info._refresh_token
            _user.profile_image = info._user_info._profile_image
            _user.add()
            return _user
        else:
            _old_user.access_token = info._access_token_info._access_token
            _old_user.expires_in = info.calcExpirationTime()
            _old_user.name = info._user_info._name
            _old_user.age = info._user_info._age
            _old_user.gender = info._user_info._gender
            _old_user.refresh_token = info._access_token_info._refresh_token
            _old_user.profile_image = info._user_info._profile_image
            _old_user.update()
            return _old_user

def save_log(info):
    pass

def make_session(info):
    session['division'] = info.division
    session['user_uid'] = info.user_uid
    if info.email:
        session['email'] = info.email
    if info.name:
        session['name'] = info.name
    if info.profile_image:
        session['profile_image'] = info.profile_image
    session['expires_in'] = info.expires_in
    print(session)

def get_refresh_token(social):
    if social == 'FACEBOOK':
        reqUrl = ''
        doReq = urllib.urlopen(reqUrl).read().decode('utf8')
        json_result = json.load(doReq)

    elif social == 'NAVER':
        reqUrl = ''
        doReq = urllib.urlopen(reqUrl).read().decode('utf8')
        json_result = json.load(doReq)

    return json_result

@user.route('/address', methods=['GET'])
def get_user_address():
    if request.method == 'GET':
        address_list = user_addr_hist_orm().query.filter_by(unique_user_id=session['user_uid']).all()
        repr_cols = ['addr_id', 'postcode', 'province', 'avenue', 'phone1', 'phone2', 'reg_date']
        result = [{ _col : getattr(_row, _col) for _col in repr_cols} for _row in address_list]
        return jsonify(result)
    elif request.method == 'POST':
        new_address = user_addr_hist_orm()
        new_address.user_uid = session['user_uid']
        new_address.postcode = request.get_json()['postcode']
        new_address.province = request.get_json()['province']
        new_address.avenue = request.get_json()['avenue']
        new_address.phone1 = request.get_json()['phone1']
        if request.get_json()['phone2']:
            new_address.phone2 = request.get_json()['phone2']
        repr_cols = ['addr_id', 'postcode', 'province', 'avenue', 'phone1', 'phone2', 'reg_date']
        result = { _col : getattr(new_address, _col) for _col in repr_cols}
        return jsonify(result)
    else:
        return ('', 404)