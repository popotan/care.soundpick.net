import json
import urllib.parse
import urllib.request
import os
from datetime import datetime, timedelta

class NaverAccessTokenInfo(object):
    _access_token = None
    _token_type = None
    _expires_in = None
    _refresh_token = None

class NaverAccessInfo(object):
    pass

class NaverUserInfo(object):
    _id = None
    _nickname = None
    _name = None
    _email = None
    _gender = None
    _age = None
    _birthday = None
    _profile_image = None

class NaverLogin(object):

    _redirect_url = None
    _user_info = None
    _access_token_info = None

    ACCESS_TOKEN_URL = 'https://nid.naver.com/oauth2.0/token?client_id={}&client_secret={}&grant_type=authorization_code&state={}&code={}'
    OPEN_API_URL = 'https://openapi.naver.com/v1/nid/me'
    REFRESH_TOKEN_URL = 'https://nid.naver.com/oauth2.0/token?grant_type=refresh_token&client_id={}&client_secret={}&refresh_token={}'

    CLIENT_ID = 'mtew_n00aY7MvmXLOyPF'
    CLIENT_SECRET = 'y1Pemk1_Ez'

    def __init__(self):
        print('네이버 로그인 초기화')

    @property
    def redirect_url(self):
        return urllib.parse.unquote(self._redirect_url)

    @redirect_url.setter
    def redirect_url(self, redirect_url):
        self._redirect_url = urllib.parse.quote(redirect_url, safe='')

    def exchangeAccessToken(self, state, code):
        reqUrl = self.ACCESS_TOKEN_URL.format(self.CLIENT_ID, self.CLIENT_SECRET, state, code)
        req = urllib.request.Request(reqUrl)
        result = urllib.request.urlopen(req).read().decode('utf8')
        result = json.loads(result)
        print(result)
        self._access_token_info = NaverAccessTokenInfo()
        self._access_token_info._access_token = result['access_token']
        self._access_token_info._refresh_token = result['refresh_token']
        self._access_token_info._token_type = result['token_type']
        self._access_token_info._expires_in = result['expires_in']
        return self

    def refreshAccesToken(self, refresh_token):
        reqUrl = self.REFRESH_TOKEN_URL.format(self.CLIENT_ID, self.CLIENT_SECRET, refresh_token)
        req = urllib.request.Request(reqUrl)
        result = urllib.request.urlopen(req).read().decode('utf8')
        result = json.loads(result)
        return self

    def getUserInfo(self):
        reqUrl = self.OPEN_API_URL
        req = urllib.request.Request(reqUrl, headers={"Authorization" : 'Bearer '+self._access_token_info._access_token, "content-type" : "application/json"})
        result = urllib.request.urlopen(req).read().decode('utf8')
        result = json.loads(result)

        self._user_info = NaverUserInfo()
        self._user_info._id = result['response']['id']
        self._user_info._nickname = result['response']['nickname']
        self._user_info._name = result['response']['name']
        self._user_info._email = result['response']['email']
        self._user_info._gender = result['response']['gender']
        self._user_info._age = result['response']['age']
        self._user_info._birthday = result['response']['birthday']
        self._user_info._profile_image = result['response']['profile_image']
        return self

    def calcExpirationTime(self):
        _timestamp =int(self._access_token_info._expires_in)
        return datetime.utcnow() + timedelta(hours=9) + timedelta(seconds=_timestamp)