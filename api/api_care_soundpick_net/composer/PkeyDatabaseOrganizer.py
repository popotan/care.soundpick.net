from flask import session
from api_care_soundpick_net.composer.DatabaseOrganizer_abc import DatabaseOrganizer
from enum import Enum
import re

from api_care_soundpick_net.model.pkey import pkey_orm
from api_care_soundpick_net.model.prod_cat import prod_cat_orm
from api_care_soundpick_net.model.prod_info import prod_info_orm
from api_care_soundpick_net.model.pkey_hist import pkey_hist_orm

class PkeyState(Enum):
    CANCEL = 0
    REGIST = 1
    RECEIPT = 2
    PAID = 3
    REFUND = 3

class Pkey(object):
    category = ''
    aver_min = ''
    aver_max = ''
    unique_serial = ''
    validation_number = ''

class PkeyDatabaseOrganizer(DatabaseOrganizer):
    pkey = None
    product = None
    def __init__(self):
        self.pkey = None
        self.product = None

    def log(self, targ_table_name, targ_val, user_div, user_uid):
        if user_div is None:
            user_div = session['division']
        if user_uid is None:
            user_uid = session['user_uid']

        _nlog = pkey_hist_orm()
        _nlog.targ_table = targ_table_name
        _nlog.targ_id = targ_val
        _nlog.exec_user_div = user_div
        _nlog.exec_user_uid = user_uid
        self.pkey.pkey_hist.append(_nlog)
        self.pkey.update()

    @staticmethod
    def parse_pkey_str(pkey_str):
        _p = re.compile('(\w{4})[-](\w{4})[-](\w{4})[-](\w{3})(\w{1})')
        _m = _p.match(pkey_str)
        if _m:
            _o = Pkey()
            _o.category = _m.group(1).upper()
            _o.aver_min = _m.group(2).upper()
            _o.aver_max = _m.group(3).upper()
            _o.unique_serial = _m.group(4).upper()
            _o.validation_number = _m.group(5).upper()
            return _o
        else:
            raise Exception('TARGET_STRING_IS_NOT_REGULATED')

    def set_by_pkey_uid(self, _id):
        _i = pkey_orm().query.filter_by(pkey_id=_id)
        print(_i)
        _i = _i.first()
        print(_i)
        if _i:
            self.pkey = _i
            return self
        else:
            raise Exception("GIVEN_PRODUCT_KEY_ID_IS_NOT_EXIST")
        
    def set_by_pkey_str(self, pkey_str):
        _m = self.parse_pkey_str(pkey_str)
        _i = pkey_orm().query\
            .filter_by(category=_m.category)\
            .filter_by(aver_min=_m.aver_min)\
            .filter_by(aver_max=_m.aver_max)\
            .filter_by(unique_serial=_m.unique_serial)\
            .filter_by(validation_number=_m.validation_number)\
            .first()
        if _i:
            self.pkey = _i
            return self
        else:
            raise Exception('GIVEN_SERIAL_IS_NOT_EXIST')
            
    def add(self, pkey_str):   #pkey db에 생성된 pkey 등록
        _i = pkey_orm()
        _p = self.parse_pkey_str(pkey_str)
        _i.category = _p.category
        _i.aver_min = _p.aver_min
        _i.aver_max = _p.aver_max
        _i.unique_serial = _p.unique_serial
        _i.validation_number = _p.validation_number
        _i.add()
        return self

    def regist(self):  #사용자로부터 pkey 사용등록 받기
        self.log('pkey_hist', 0)
        return self

    def get_prod_info(self):
        _i = prod_cat_orm().query.filter_by(category=self.pkey.category).first()
        if _i:
            _i2 = prod_info_orm().query.filter_by(prod_id=_i.prod_id).first()
            if _i2:
                self.product = _i2
                return self
            else:
                raise Exception('PRODUCT_COMMON_INFO_IS_NOT_EXIST')
        else:
            raise Exception('PRODUCT_CATEGORY_INFO_IS_NOT_EXIST')

    def is_regist(self):
        if len(self.pkey.pkey_hist):
            return True
        else:
            return False