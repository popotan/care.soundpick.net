import json
import urllib.parse
import urllib.request
import os
from datetime import datetime, timedelta

class ImpPayment(object):
    IMP_UID = ''

    GET_TOKEN_URL = "https://api.iamport.kr/users/getToken"
    GET_PAYMENT_RESULT_URL = "https://api.iamport.kr/payments/{}" #imp_uid
    CANCEL_PAYMENT_URL = "https://api.iamport.kr/payments/cancel"
    GET_LIST_BY_STATUS_URL = "https://api.iamport.kr/payments/status/{}"
    GET_LIST_BY_MERCHANT_ID_URL = "https://api.iamport.kr/payments/findAll/{}/{}"
    RECEIPTS_URL = "https://api.iamport.kr/receipts/{}"

    MERCHANT_INFO = json.dumps({
            "imp_key" : "0394308613461714",
            "imp_secret" : "lTzNCODD9A6vEV7KfwnrgvRDB5hqezxzt7m5V0I5BOAfKH00ensuSj8uoqUD0sctzzyvAgAb5aqQSqQ7" 
        }).encode('utf8')
    IMP_HEADER = {
        "Authorization" : "",
        "Content-Type" : "application/json"
    }

    TOKEN_INFO = {}
    _result = {}

    def __init__(self, imp_uid):
        self.IMP_UID = imp_uid
        self.getToken()

    def getToken(self):
        reqUrl = self.GET_TOKEN_URL
        req = urllib.request.Request(reqUrl, \
                data=self.MERCHANT_INFO, \
                method="POST", \
                headers=self.IMP_HEADER)
        result = urllib.request.urlopen(req).read().decode('utf8')
        result = json.loads(result)
        self.TOKEN_INFO = result
        print(result)
        if self.TOKEN_INFO["code"] == 0:
            self.IMP_HEADER["Authorization"] = self.TOKEN_INFO["response"]["access_token"]
        else:
            raise Exception("IMP로부터 API사용을 위한 ACCESS_TOKEN 발급에 실패하였습니다.");

    def getPaymentResult(self):
        reqUrl = self.GET_PAYMENT_RESULT_URL.format(self.IMP_UID)
        req = urllib.request.Request(reqUrl, \
                method="GET", \
                headers=self.IMP_HEADER)
        result = urllib.request.urlopen(req)\
                .read().decode('utf8')
        result = json.loads(result)
        self._result = result
        return self

    def cancel(self, \
                merchant_uid=None, \
                amount=None, \
                reason="", \
                refund_holder="", \
                refund_bank="", \
                refund_account=""):
        refund_info = {
            "imp_uid" : self.IMP_UID,
            "merchant_uid" : merchant_uid,
            "amount" : amount,
            "reason" : reason,
            "refund_holder" : refund_holder,
            "refund_bank" : refund_bank,
            "refund_account" : refund_account
        }
        reqUrl = self.CANCEL_PAYMENT_URL
        req = urllib.request.Request(reqUrl, \
                method="POST", \
                headers=self.IMP_HEADER, \
                data=refund_info)
        result = urllib.request.urlopen(req)\
                .read().decode('utf8')
        result = json.loads(result)
        self._result = result
        return self

    def getListByStatus(self, status='all', page=1, from_time=0, to_time=0):
        reqUrl = self.GET_LIST_BY_STATUS_URL.format(status)
        req = urllib.request.Request(reqUrl, \
                method="", \
                headers=self.IMP_HEADER)
        result = urllib.request.urlopen(req)\
                .read().decode('utf8')
        result = json.loads(result)
        self._result = result
        return self

    def getListByMerchatId(self, merchant_uid=None, status='paid', page=1):
        reqUrl = self.GET_LIST_BY_MERCHANT_ID_URL.format(merchant_uid, status)
        req = urllib.request.Request(reqUrl, \
                method="GET", \
                headers=self.IMP_HEADER)
        result = urllib.request.urlopen(req)\
                .read().decode('utf8')
        result = json.loads(result)
        self._result = result
        return self

    def getReceipts(self, \
                    identifier='', \
                    target_type='', \
                    buyer_name='', \
                    buyer_email='', \
                    buyer_tel='', \
                    vat=10):
        receipts_info = {
            "imp_uid" : self.IMP_UID,
            "identifier" : identifier,
            "type" : target_type,
            "buyer_name" : buyer_name,
            "buyer_email" : buyer_email,
            "buyer_tel" : buyer_tel,
            "vat" : vat
        }
        reqUrl = self.RECEIPTS_URL.format(self.RECEIPTS_URL)
        req = urllib.request.Request(reqUrl, \
                method="POST", \
                headers=self.IMP_HEADER, \
                data=receipts_info)
        result = urllib.request.urlopen(req)\
                .read().decode('utf8')
        result = json.loads(result)
        self._result = result
        return self