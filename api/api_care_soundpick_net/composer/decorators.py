from functools import wraps
from flask import session

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'is_logged_in' in session.keys():
         if session['is_logged_in']:
         	return f(*args, **kwargs)
        return ('', 401)
    return decorated_function
    
def login_required_admin(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'division' in session.keys():
         if session['division'] == 'admin':
         	return f(*args, **kwargs)
        return ('', 401)
       
    return decorated_function