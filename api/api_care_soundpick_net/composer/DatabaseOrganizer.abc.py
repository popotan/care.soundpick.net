import abc

class DatabaseOrganizer:
    __metaclass__ = abc.ABCMeta

    @abc.abstractstaticmethod
    def log(self):
        pass

    @abc.abstractclassmethod
    def set_by_pkey_uid(self):
        pass

    @abc.abstractclassmethod
    def set_by_pkey_str(self):
        pass
