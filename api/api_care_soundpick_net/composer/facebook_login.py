import json
import urllib.parse
import urllib.request
import os
from datetime import datetime, timedelta

class FacebookAccessTokenInfo(object):
    _access_token = None
    _token_type = None
    _expires_in = None

class FacebookAppAccessTokenInfo(object):
    _access_token = None
    _token_type = None

class FacebookAccessInfo(object):
    _app_id = None
    _application = None
    _expires_at = None
    _is_valid = None
    _issued_at = None
    _scopes = None
    _user_id = None

class FacebookUserInfo(object):
    _id = None
    _email = None
    _name = None
    _gender = None
    _age_range = None
    _profile_image_url = None

class FacebookLogin(object):
    
    LOGIN_DIALOG_URL = 'https://www.facebook.com/v2.9/dialog/oauth?client_id={}&redirect_uri={}' #use format(app-id,redirect-uri)
    LOGIN_DENY_URL = '{}?error_reason={}&error={}&error_description={}' #use format(YOUR_REDIRECT_URI,error_reason,error,error_description)
    ACCESS_TOKEN_URL = 'https://graph.facebook.com/v2.9/oauth/access_token?client_id={}&redirect_uri={}&client_secret={}&code={}' #use format(app-id,redirect-uri,app-secret,code-parameter)
    DEBUG_TOKEN_URL = 'https://graph.facebook.com/debug_token?input_token={}&access_token={}' #use format(token-to-inspect,app-token-or-admin-token)
    REQUEST_PERMISSION_URL = 'https://www.facebook.com/v2.9/dialog/oauth?client_id={}&redirect_uri={}&auth_type=rerequestscope={}' #use format(app-id,redirect-uri,scope)
    APP_ACCESS_TOKEN_URL = 'https://graph.facebook.com/oauth/access_token?client_id={}&client_secret={}&grant_type=client_credentials' #use format(app-id, client-secret)
    GRAPH_API_URL = 'https://graph.facebook.com/me?access_token={}&fields={}' #use format(_access_token_info._access_token, "name,email,gender,age_range")

    CLIENT_ID = '243422466138379'
    CLIENT_SECRET = '70f3d091d3cadfd87e21a27732524849'

    _redirect_url = None
    _access_token_info = None
    _app_access_token_info = None
    _access_info = None
    _user_info = None

    def __init__(self):
        print('페이스북 로그인 초기화')

    @property
    def redirect_url(self):
        return urllib.parse.unquote(self._redirect_url)

    @redirect_url.setter
    def redirect_url(self, redirect_url):
        self._redirect_url = urllib.parse.quote(redirect_url, safe='')

    def exchangeAccessToken(self, code):
        reqUrl = self.ACCESS_TOKEN_URL.format(self.CLIENT_ID, self._redirect_url, self.CLIENT_SECRET, code)
        req = urllib.request.Request(reqUrl)
        result = urllib.request.urlopen(req).read().decode('utf8')
        # print('exchangeAccessToken Result ->>')
        # print(result)
        result = json.loads(result)
        self._access_token_info = FacebookAccessTokenInfo()
        self._access_token_info._access_token = result['access_token']
        return self
    
    def debugAccessToken(self):
        self.getAppAccessToken()
        reqUrl = self.DEBUG_TOKEN_URL.format(self._access_token_info._access_token, self._app_access_token_info._access_token)
        # print('debugAccessToken reqUrl ->>')
        # print(reqUrl)
        req = urllib.request.Request(reqUrl)
        result = urllib.request.urlopen(req).read().decode('utf8')  #['data']로 시작
        # print('debugAccessToken Result ->>')
        # print(result)
        result = json.loads(result)
        self._access_info = FacebookAccessInfo()
        self._access_info._app_id = result['data']['app_id']
        self._access_info._application = result['data']['application']
        self._access_info._expires_at = result['data']['expires_at']
        self._access_info._is_valid = result['data']['is_valid']
        self._access_info._issued_at = result['data']['issued_at']
        self._access_info._scopes = result['data']['scopes']
        self._access_info._user_id = result['data']['user_id']
        return self

    def getAppAccessToken(self):
        reqUrl = self.APP_ACCESS_TOKEN_URL.format(self.CLIENT_ID, self.CLIENT_SECRET)
        req = urllib.request.Request(reqUrl)
        result = urllib.request.urlopen(req).read().decode('utf8')
        # print('AppAccessToken Result ->>')
        # print(result)
        result = json.loads(result)   #['access_token'] 부분 파싱하면 됨.
        self._app_access_token_info = FacebookAppAccessTokenInfo()
        self._app_access_token_info._access_token = result['access_token']
        self._app_access_token_info._token_type = result['token_type']

    def getUserInfo(self):
        reqUrl = self.GRAPH_API_URL.format(self._access_token_info._access_token, 'name,email,gender,age_range')
        req = urllib.request.Request(reqUrl)
        result = urllib.request.urlopen(req).read().decode('utf8')
        # print('userInfo Result ->>')
        # print(result)
        result = json.loads(result)
        self._user_info = FacebookUserInfo()
        self._user_info._id = result['id']
        self._user_info._email = result['email']
        self._user_info._name = result['name']
        self._user_info._gender = result['gender']
        self._user_info._age_range = result['age_range']
        self._user_info._profile_image_url = self.getUserProfileImageUrl()
        return self

    def getUserProfileImageUrl(self):
        return 'https://graph.facebook.com/{}/picture?type=large'.format(self._access_info._user_id)

    def calcExpirationTime(self):
        # _timestamp = datetime.fromtimestamp(self._access_info._issued_at) + datetime.fromtimestamp(self._access_info._expires_at)
        _timestamp = datetime.fromtimestamp(self._access_info._expires_at)
        return _timestamp