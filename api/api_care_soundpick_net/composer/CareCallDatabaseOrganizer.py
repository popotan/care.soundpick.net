from api_care_soundpick_net.composer.CareDatabaseOrganizer import CareDatabaseOrganizer

from api_care_soundpick_net.model.care import care_orm
from api_care_soundpick_net.model.care_hist import care_hist_orm
from api_care_soundpick_net.model.care_call import care_call_orm
from api_care_soundpick_net.model.care_call_hist import care_call_hist_orm
from api_care_soundpick_net.model.care_call_dlvr_hist import care_call_dlvr_hist_orm
from api_care_soundpick_net.model.care_order import care_order_orm
from api_care_soundpick_net.model.care_wrty_hist import care_wrty_hist_orm
from api_care_soundpick_net.model.imp_order_resp import imp_order_resp_orm
from api_care_soundpick_net.model.imp_order_resp_cncl import imp_order_resp_cncl_orm

class CareCallDatabaseOrganizer(CareDatabaseOrganizer):
    call = None
    def __init__(self):
        super.__init__()
        self.call = None

    def add(self, reason, user_addr_hist_id):
        _i = care_call_orm()
        _i.care_id = self.care.care_id
        _i.reason = reason
        _i.user_addr_hist_id = user_addr_hist_id
        _i.add()
        super.log(_i.__tablename__, _i.call_id)
        return self

    def regist(call_id, state, rslt_val):
        _i = care_call_hist_orm()
        _i.call_id = call_id
        _i.state = state
        _i.rslt_val = rslt_val
        _i.add()
        super.log(_i.__tablename__, _i.hist_id)
        return self
        
    def set_dlvr_info(self, call_id):
        _i = care_call_dlvr_hist_orm()
        _i.call_id = call_id
        _i.dlvr_cpny = dlvr_cpny
        _i.dlvr_num = dlvr_num
        _i.pay_type = pay_type
        _i.direction = direction
        _i.add()
        super.log(_i.__tablename__, hist_id)
        return self