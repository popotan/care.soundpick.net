from flask import session

from api_care_soundpick_net.composer.CareDatabaseOrganizer import CareDatabaseOrganizer

from api_care_soundpick_net.model.care import care_orm
from api_care_soundpick_net.model.care_hist import care_hist_orm
from api_care_soundpick_net.model.care_call import care_call_orm
from api_care_soundpick_net.model.care_call_hist import care_call_hist_orm
from api_care_soundpick_net.model.care_call_dlvr_hist import care_call_dlvr_hist_orm
from api_care_soundpick_net.model.care_order import care_order_orm
from api_care_soundpick_net.model.care_wrty_hist import care_wrty_hist_orm
from api_care_soundpick_net.model.imp_order_resp import imp_order_resp_orm
from api_care_soundpick_net.model.imp_order_resp_cncl import imp_order_resp_cncl_orm
from api_care_soundpick_net.model.care_order_reg_hist import care_order_reg_hist_orm

class CareOrderDatabaseOrganizer(CareDatabaseOrganizer):
    def __init__(self):
        super().__init__()

    def set_amount(self, amount):
        _i = care_order_orm()
        _i.due_amount = amount
        self.pkey.care[0].care_order.append(_i)
        self.pkey.update()
        return self

    def get_amount(self, care_order_id):
        _i = care_order_orm().query\
        .filter_by(order_id=care_order_id)\
        .filter_by(care_id=self.pkey.care[0].care_id).first()
        if _i:
            return _i.due_amount
        else:
            raise Exception('ORDER_ID_IS_NOT_EXIST')

    def set_info(self, care_order_id, payment_info, user_div, user_uid):
        _i = care_order_orm().query.filter_by(order_id=care_order_id).first()
        if _i:
            _i.care_id = self.pkey.care[0].care_id
            _i.code = payment_info["code"]
            _i.message = payment_info["message"]

            if "response" in payment_info:
                _r = imp_order_resp_orm().query.filter_by(imp_uid=payment_info['response']['imp_uid']).first()
                _old_r = _r
                if not _r: _r = imp_order_resp_orm()
                for _pn, _pv in payment_info["response"].items():
                    if hasattr(_r, _pn):
                        setattr(_r, _pn, _pv)

                if not _old_r:
                    _i.imp_order_resp.append(_r)
                else:
                    _r.update()
        
            _i.update()

            self.log(_i.__tablename__, _i.order_id, user_div, user_uid)
            self.log(_r.__tablename__, _r.resp_id, user_div, user_uid)
            return self
        else:
            raise Exception('ORDER_ID_IS_NOT_EXIST')

    def get_imp_info(self, care_order_id):
        _i = care_order_orm().query.filter_by(order_id=care_order_id).first()
        if _i:
            return _i.imp_order_resp
        else:
            raise Exception('ORDER_ID_IS_NOT_EXIST')

    def regist_order_hist(self, state):
        crh = care_order_reg_hist_orm()
        crh.pkey_id = self.pkey.pkey_id
        crh.state = state
        crh.add()
        return self