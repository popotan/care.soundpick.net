from flask import session

from api_care_soundpick_net.composer.PkeyDatabaseOrganizer import PkeyDatabaseOrganizer

from api_care_soundpick_net.model.care import care_orm
from api_care_soundpick_net.model.care_hist import care_hist_orm
from api_care_soundpick_net.model.care_call import care_call_orm
from api_care_soundpick_net.model.care_call_hist import care_call_hist_orm
from api_care_soundpick_net.model.care_call_dlvr_hist import care_call_dlvr_hist_orm
from api_care_soundpick_net.model.care_order import care_order_orm
from api_care_soundpick_net.model.care_wrty_hist import care_wrty_hist_orm
from api_care_soundpick_net.model.imp_order_resp import imp_order_resp_orm
from api_care_soundpick_net.model.imp_order_resp_cncl import imp_order_resp_cncl_orm

class CareGrade(object):
    COMMON = 0
    BASIC = 1
    PLUS = 2

class CareDatabaseOrganizer(PkeyDatabaseOrganizer):
    def __init__(self):
        super().__init__()

    def add(self, receipt_filename, user_div, user_uid):   #care와 pkey연결
        _i = care_orm()

        if len(self.pkey.care) > 0:
            if 'user_uid' in session:
                self.pkey.care[0].user_uid = session['user_uid']
            self.pkey.care[0].receipt_filename = receipt_filename
        else:
            if 'user_uid' in session:
                _i.user_uid = session['user_uid']
            _i.receipt_filename = receipt_filename
            self.pkey.care.append(_i)
        self.pkey.update()

        self.log(_i.__tablename__, self.pkey.care[0].care_id, user_div, user_uid)
        return self

    def regist(self, state, rslt_val, user_div, user_uid):
        _nlog = care_hist_orm()
        _nlog.care_id = self.pkey.care[0].care_id
        _nlog.state = state
        _nlog.rslt_val = rslt_val
        _nlog.add()
        self.log(_nlog.__tablename__, _nlog.hist_id, user_div, user_uid)
        return self

    def set_by_care_uid(self, _id):
        _i = care_orm().query.filter_by(care_id=_id).first()
        if _i:
            self.set_by_pkey_uid(_i.pkey_id)
            return self
        else:
            raise Exception('GIVEN_CARE_KEY_ID_IS_NOT_EXIST')

    def set_wrty_info(self, wrty_type, from_date, end_date, user_div, user_uid):
        _i = care_wrty_hist_orm()
        _i.care_id = self.pkey.care[0].care_id
        _i.wrty_type = wrty_type
        _i.s_date = from_date
        _i.e_date = end_date
        _i.add()
        self.log(_i.__tablename__, _i.hist_id, user_div, user_uid)
        return self

    def get_wrty_state(self):
        _f_val = 'INVALID'
        if len(self.pkey.care):
            if len(self.pkey.care[0].care_wrty_hist):
                for _ch in self.pkey.care[0].care_wrty_hist:
                    _f_val = _ch.wrty_type
        return _f_val

    def is_wrty_valid(self):
        _i = self.pkey.care[0].care_wrty_hist[-1]
        _now = datetime.utcnow() + timedelta(hours=9)
        if _i.s_date < _now and _i.e_date > _now:
            return True
        else:
            return False