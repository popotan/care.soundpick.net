# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from api_care_soundpick_net import db
from api_care_soundpick_net.model.care_order import care_order_orm
from api_care_soundpick_net.model.care_hist import care_hist_orm
from api_care_soundpick_net.model.care_wrty_hist import care_wrty_hist_orm
from api_care_soundpick_net.model.care_call import care_call_orm

from datetime import datetime, timedelta

class care_orm(db.Model):
    __bind_key__ = 'care_service'
    __tablename__ = 'care'
    care_id = Column(Integer,primary_key=True,unique=True)
    pkey_id = Column(Integer, db.ForeignKey('pkey.pkey_id'))
    user_uid = Column(Integer, default=0)
    receipt_filename = Column(String(200))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    care_order = db.relationship(care_order_orm, backref="care")
    care_hist = db.relationship(care_hist_orm, backref="care")
    care_wrty_hist = db.relationship(care_wrty_hist_orm, backref="care")
    care_call = db.relationship(care_call_orm, backref="care")

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()