# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from api_care_soundpick_net import db

from datetime import datetime, timedelta

class imp_order_resp_cncl_orm(db.Model):
    __bind_key__ = 'care_service'
    __tablename__ = 'imp_order_resp_cncl'
    cncl_id = Column(Integer,primary_key=True,unique=True)
    imp_uid = Column(String(200),db.ForeignKey('imp_order_resp.imp_uid'))
    pg_tid = Column(String(200))
    amount = Column(Integer)
    cancelled_at = Column(Integer)
    reason = Column(String(200))
    receipt_url = Column(String(1000))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()