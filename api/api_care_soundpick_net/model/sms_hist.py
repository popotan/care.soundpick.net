# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from api_care_soundpick_net import db

from datetime import datetime, timedelta

class sms_hist_orm(db.Model):
    __bind_key__ = 'sp_admin_V2'
    __tablename__ = 'sms_hist'
    hist_id = Column(Integer,primary_key=True,unique=True)
    user_uid = Column(Integer)
    targ_num = Column(String(45))
    msg = Column(String(1000))
    result = Column(String(45))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()