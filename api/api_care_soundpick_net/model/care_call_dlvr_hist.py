# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from api_care_soundpick_net import db

from datetime import datetime, timedelta

class care_call_dlvr_hist_orm(db.Model):
    __bind_key__ = 'care_service'
    __tablename__ = 'care_call_dlvr_hist'
    hist_id = Column(Integer,primary_key=True,unique=True)
    call_id = Column(Integer,db.ForeignKey('care_call.call_id'))
    dlvr_cpny = Column(String(45))
    dlvr_num = Column(String(45))
    pay_type = Column(String(45))
    direction = Column(String(45))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()