# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from api_care_soundpick_net import db
from api_care_soundpick_net.model.prod_cat import prod_cat_orm

from datetime import datetime, timedelta

class prod_info_orm(db.Model):
    __bind_key__ = 'care_service'
    __tablename__ = 'prod_info'
    prod_id = Column(Integer,primary_key=True,unique=True)
    name = Column(String(45))
    brand = Column(String(45))
    model = Column(String(45))
    thumbnail_url = Column(String(200))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    prod_cat = db.relationship(prod_cat_orm, backref="prod_info")

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()