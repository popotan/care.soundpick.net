# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from api_care_soundpick_net import db

from datetime import datetime, timedelta

class pkey_hist_orm(db.Model):
    __bind_key__ = 'care_service'
    __tablename__ = 'pkey_hist'
    hist_id = Column(Integer, primary_key=True, unique=True)
    pkey_id = Column(Integer, db.ForeignKey('pkey.pkey_id'))
    targ_table = Column(String(45))
    targ_id = Column(Integer)
    exec_user_div = Column(String(45))
    exec_user_uid = Column(Integer)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()