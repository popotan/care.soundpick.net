# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from api_care_soundpick_net import db
from api_care_soundpick_net.model.pkey import pkey_orm

from datetime import datetime, timedelta

class prod_cat_orm(db.Model):
    __bind_key__ = 'care_service'
    __tablename__ = 'prod_cat'
    cat_id = Column(Integer,primary_key=True,unique=True)
    prod_id = Column(Integer, db.ForeignKey('prod_info.prod_id'))
    category = Column(String(4), unique=True)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    pkey = db.relationship(pkey_orm, backref="prod_cat")

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()