# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from api_care_soundpick_net import db
from api_care_soundpick_net.model.imp_order_resp_cncl import imp_order_resp_cncl_orm

from datetime import datetime, timedelta
import json

class imp_order_resp_orm(db.Model):
    __bind_key__ = 'care_service'
    __tablename__ = 'imp_order_resp'
    resp_id        = Column(Integer,primary_key=True,unique=True)
    order_id       = Column(Integer,db.ForeignKey('care_order.order_id'))
    imp_uid        = Column(String(200),unique=True)
    merchant_uid   = Column(String(200))
    pay_method     = Column(String(45))
    pg_provider    = Column(String(45))
    pg_tid         = Column(String(200))
    escrow         = Column(Boolean)
    apply_num      = Column(String(200))
    bank_name      = Column(String(45))
    card_name      = Column(String(45))
    card_quota     = Column(Integer)
    vbank_name     = Column(String(45))
    vbank_num      = Column(String(200))
    vbank_holder   = Column(String(45))
    vbank_date     = Column(Integer)
    name           = Column(String(45))
    amount         = Column(Integer)
    cancel_amount  = Column(Integer)
    currency       = Column(String(45))
    buyer_name     = Column(String(45))
    buyer_email    = Column(String(200))
    buyer_tel      = Column(String(45))
    buyer_addr     = Column(String(200))
    buyer_postcode = Column(String(45))
    _custom_data    = Column('custom_data', String(1000))
    user_agent     = Column(String(200))
    status         = Column(String(45))
    paid_at        = Column(Integer)
    failed_at      = Column(Integer)
    cancelled_at   = Column(Integer)
    fail_reason    = Column(String(200))
    cancel_reason  = Column(String(200))
    receipt_url    = Column(String(1000))
    reg_date       = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    imp_order_resp_cncl = db.relationship(imp_order_resp_cncl_orm, backref="imp_order_resp")

    @property
    def custom_data(self):
        if self._custom_data:
            return json.loads(self._custom_data)
        else:
            return None

    @custom_data.setter
    def custom_data(self, data):
        if not isinstance(data, str):
            self._custom_data = str(data)
        else:
            self._custom_data = data

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()