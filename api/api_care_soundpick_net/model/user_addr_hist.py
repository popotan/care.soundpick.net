# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from api_care_soundpick_net import db

from datetime import datetime, timedelta

class user_addr_hist_orm(db.Model):
    __bind_key__ = 'care_service'
    __tablename__ = 'user_addr_hist'
    addr_id = Column(Integer,primary_key=True,unique=True)
    user_uid = Column(Integer, db.ForeignKey('user.user_uid'), default=0)
    postcode = Column(String(45))
    province = Column(String(45))
    avenue = Column(String(45))
    phone1 = Column(String(45))
    phone2 = Column(String(45))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()