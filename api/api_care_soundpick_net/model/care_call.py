# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from api_care_soundpick_net import db
from api_care_soundpick_net.model.care_call_hist import care_call_hist_orm
from api_care_soundpick_net.model.care_call_dlvr_hist import care_call_dlvr_hist_orm

from datetime import datetime, timedelta

class care_call_orm(db.Model):
    __bind_key__ = 'care_service'
    __tablename__ = 'care_call'
    call_id = Column(Integer,primary_key=True,unique=True)
    care_id = Column(Integer,db.ForeignKey('care.care_id'))
    reason = Column(String(200))
    user_addr_hist_id = Column(Integer)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    care_call_hist = db.relationship(care_call_hist_orm, backref="care_call")
    care_call_dlvr_hist = db.relationship(care_call_dlvr_hist_orm, backref="care_call")

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()