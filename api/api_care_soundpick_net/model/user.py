# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from api_care_soundpick_net import db
from api_care_soundpick_net.model.user_addr_hist import user_addr_hist_orm

from datetime import datetime, timedelta

class user_orm(db.Model):
    __bind_key__ = 'care_service'
    __tablename__ = 'user'
    user_uid = Column(Integer,primary_key=True,unique=True)
    user_id = Column(String(200))
    division = Column(String(45), default='GUEST')
    phone_num = Column(String(45), unique=True)
    email = Column(String(200))
    name = Column(String(45))
    age = Column(String(45))
    birth = Column(String(45))
    gender = Column(String(45))
    access_token = Column(String(1000))
    refresh_token = Column(String(1000))
    expires_in = Column(DateTime)
    profile_image = Column(String(1000))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    user_addr_hist = db.relationship(user_addr_hist_orm, backref="user")

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()