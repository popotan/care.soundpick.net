# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship, backref
from flask_sqlalchemy import SQLAlchemy

from api_care_soundpick_net import db
from api_care_soundpick_net.model.pkey_hist import pkey_hist_orm
from api_care_soundpick_net.model.care import care_orm

from datetime import datetime, timedelta

class pkey_orm(db.Model):
    __bind_key__ = 'care_service'
    __tablename__ = 'pkey'
    pkey_id = Column(Integer,primary_key=True,unique=True)
    category = Column(String(4), db.ForeignKey("prod_cat.category"))
    aver_min = Column(String(4))
    aver_max = Column(String(4))
    unique_serial = Column(String(3))
    validation_number = Column(String(1))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    pkey_hist = db.relationship(pkey_hist_orm, backref="pkey")
    care = db.relationship(care_orm, backref="pkey")

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()
