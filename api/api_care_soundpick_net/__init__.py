from flask import Flask, send_from_directory, session, request, render_template, g
from flask_session import Session
from flask_bcrypt import Bcrypt

from api_care_soundpick_net.config import database
from datetime import timedelta

import os

app = Flask(__name__, template_folder='template', static_url_path='/static')
app.secret_key = 'api_care_soundpick_net'

app.config.from_object(database)
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()
db.init_app(app)

# from flask_socketio import SocketIO, emit
# from gevent.monkey import patch_all
# patch_all()
# socketio = SocketIO(app,\
#  					manage_session=False,\
#  					async_mode="gevent",\
#  					engineio_logger=True)

#쿠키관련 설정
app.config.update(
	SESSION_TYPE='sqlalchemy',
	SESSION_COOKIE_NAME='sp_care',
	SESSION_COOKIE_HTTPONLY=False,
	SESSION_COOKIE_SECURE=False,
	PERMANENT_SESSION_LIFETIME=timedelta(seconds=600),
	SESSION_REFRESH_EACH_REQUEST=True
	)
Session(app)

bcrypt = Bcrypt(app)

# @socketio.on('client_connected')
# def handle_client_connect_event(json):
# 	print('client_connected')
# 	print('received json: {0}'.format(str(json)))

# @socketio.on('connect')
# def is_connected():
# 	print('user was connected')

# @socketio.on('connection')
# def connecting():
# 	print(session)
# 	emit('init_response', {'data':'socket was connected.'})

# @socketio.on('disconnect')
# def session_expire():
# 	print('socket was disconnected')
# 	try:
# 		if session['division'] is 'GUEST' and request.referrer.find('/login') < 0:
# 			session.clear()
# 			print('session expired')
# 		else:
# 			pass
# 	except Exception as e:
# 		pass

@app.route('/static/<path:path>')
def send_js(path):
	"""
		static 파일들을 템플릿상에서 url_for 없이 사용하기 위해 라우팅 제공
	"""
	return send_from_directory('static/', path)

@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response

from api_care_soundpick_net.controller.user import user
from api_care_soundpick_net.controller.keygen import keygen
from api_care_soundpick_net.controller.care import care
from api_care_soundpick_net.controller.payment import payment
from api_care_soundpick_net.controller.serial import serial

app.register_blueprint(user, url_prefix='/api/user')
app.register_blueprint(keygen, url_prefix='/api/keygen')
app.register_blueprint(care, url_prefix='/api/care')
app.register_blueprint(payment, url_prefix='/api/payment')
app.register_blueprint(serial, url_prefix='/api/serial')