import { Care.Soundpick.NetPage } from './app.po';

describe('care.soundpick.net App', () => {
  let page: Care.Soundpick.NetPage;

  beforeEach(() => {
    page = new Care.Soundpick.NetPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
